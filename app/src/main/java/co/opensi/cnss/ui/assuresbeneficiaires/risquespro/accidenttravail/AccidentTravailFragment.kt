package co.opensi.cnss.ui.assuresbeneficiaires.risquespro.accidenttravail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_pension_details.view.*
import kotlinx.android.synthetic.main.fragment_prestations_risques_pro.view.*
import kotlinx.android.synthetic.main.fragment_procedure_prise_en_charge.view.*
import kotlinx.android.synthetic.main.fragment_risques_professionnels.view.rl_accident_travail

class AccidentTravailFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val rootView  = inflater.inflate(R.layout.fragment_accident_travail, viewGroup, false)
//        requireActivity().setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        isAccidentTravail = arguments?.getBoolean("choice")!!


        if (isAccidentTravail){
            (requireActivity()as HomeActivity).setActionBarTitle(  "Accident de travail")
        }else{
            (requireActivity()as HomeActivity).setActionBarTitle( "Maladies professionnelles")
        }


        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter

        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))

        return rootView
    }



    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DefinitionFragment (defined as a static inner class below).
            lateinit var fragment : Fragment

            if (position == 0){
                fragment  =
                    DefinitionFragment.newInstance()

            }else if( position == 2){
                fragment =
                    PrestationsFragment.newInstance()
            }else{
                fragment =
                    ProcedureFragment.newInstance()
            }

            fragment.retainInstance = true
            return  fragment

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 3
        }
    }



    /**
     * A placeholder fragment containing a custom view now.
     */
    class DefinitionFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            //prevoir la variante pour les maladies pro
            val rootView = inflater.inflate(
               if(isAccidentTravail) R.layout.fragment_conditions_assurance_vieillesse else R.layout.fragment_definition_maladies_pro ,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): DefinitionFragment {
                val fragment =
                    DefinitionFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }


    class ProcedureFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                if(isAccidentTravail) R.layout.fragment_procedure_prise_en_charge else R.layout.fragment_procedure_prise_en_charge_maladies_pro,
                container,
                false
            )

            Functions.setBoldTextView(rootView.firstBoldText, requireContext(), true)
            Functions.setBoldTextView(rootView.secondBoldText, requireContext(), true)

            //prevoir la variante pour les maladies pro

            return rootView
        }

        companion object {
            fun newInstance(): ProcedureFragment {
                val fragment =
                    ProcedureFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

    class PrestationsFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_prestations_risques_pro,
                container,
                false
            )

            if (isAccidentTravail){
                rootView.rl_maladies_professionnelles.visibility = View.GONE
                rootView.rl_accident_travail.visibility = View.VISIBLE
            }else {
                rootView.rl_maladies_professionnelles.visibility = View.VISIBLE
                rootView.rl_accident_travail.visibility = View.GONE
            }

            rootView.rl_maladies_professionnelles.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentDeclarationMaladieProfessionnelle))
            }

            // le click sur une prestation ramene sur le details des prestations
            rootView.rl_accident_travail.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentAccidentTravail))
            }


            rootView.rl_continuite_soins.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentContinuiteSoins))
            }

            rootView.rl_reglement_factures.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentReglementFactures))
            }
            rootView.rl_demande_remboursement.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentDemandeRemboursements))
            }
            rootView.rl_indemnite_journaliere.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentIndemnitesJournaliers))
            }
            rootView.rl_traitement_rechutes.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentTraitementRechutes))
            }
            rootView.rl_remplacement_prothese.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentRemplacementProthese))
            }

            rootView.rl_rente.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentRente))
            }

            rootView.rl_rente_survivants.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_accidentTravailFragment_to_pensionDetailsFragment,
                    bundleOf("content" to contentRenteSurvivants))
            }



            return rootView
        }

        companion object {


            fun newInstance(): PrestationsFragment {
                val fragment =
                    PrestationsFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }


    companion object{
        var contentAccidentTravail = mutableListOf<Data>(
            Data(
                title = "Accident de travail",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Déclaration d’accident du travail "),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Liste des pièces à fournir par les survivants en cas de décès "),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Liste des Centres agréés"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Questionnaire trajet"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Questionnaire effort"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Questionnaire témoins")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat médical initial et final"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "PV de la police")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la déclaration",
                    "Etude",
                    "Vérification",
                    "Signature",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            )
        )

        var contentContinuiteSoins = mutableListOf<Data>(
            Data(
                title = "Continuité des soins",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Ordonnances"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Bons d’examens"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Prise en charge (volet N°1)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Facture pro forma")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Présentation des prescriptions médicales",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            ),
            Data(
                isActive=false
            )
        )

        var contentDeclarationMaladieProfessionnelle = mutableListOf<Data>(
            Data(
                title = "Declaration de maladies profesionnelles",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Déclaration de maladie professionnelle"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Liste des maladies professionnelles"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Liste des Centres agréés")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat médical de constatation de la maladie")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Etude",
                    "Avis du Médecin conseil",
                    "Signature",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            )
        )

        var contentReglementFactures= mutableListOf<Data>(
            Data(
                title = "Reglement des factures",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Factures originales"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Bon des examens et ordonnances")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Traitement",
                    "Vérification",
                    "Signature",
                    "Paiement"
                ),
                tabItemName = "Processus de traitement"
            ),
            Data(
                isActive=false
            )
        )

        var contentDemandeRemboursements= mutableListOf<Data>(
            Data(
                title = "Demande de remboursements",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Factures originales"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Bon des examens et ordonnances")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Traitement",
                    "Vérification",
                    "Signature",
                    "Paiement"
                ),
                tabItemName = "Processus de traitement"
            ),
            Data(
                isActive=false
            )
        )

        var contentIndemnitesJournaliers= mutableListOf<Data>(
            Data(
                title = "Indemnité journalière",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat de guérison ou de reprise de service"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Les originaux des arrêts de travail"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La prise en charge (volet N°1) remplie par l’employeur (rubrique interruption de travail)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Fiche de paie du mois précédent l’accident")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Etude",
                    "Vérification",
                    "Signature",
                    "Paiement"
                ),
                tabItemName = "Processus de traitement"
            ),
            Data(
                isActive=false
            )
        )

        var contentTraitementRechutes= mutableListOf<Data>(
            Data(
                title = "Traitement des rechutes",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Demande de rechute")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat médical de son état actuel (attestant la rechute)")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Signature",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            )
        )

        var contentRemplacementProthese= mutableListOf<Data>(
            Data(
                title = "Remplacement de prothèse",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Demande de prothèse")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Ancienne facture pro forma"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La prescription de la nouvelle prothèse"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Au moins deux (02) factures pro forma de différents Centres")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Vérification",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            )
        )


        var contentRepriseSoins = mutableListOf<Data>(
            Data(
                title = "Reprise de soins",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Demande de reprise de soins")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Prise en charge volet N°1"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Bon de consultation"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat médical de son état actuel")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Dépôt de la demande",
                    "Vérification",
                    "Etude",
                    "Avis du Médecin conseil",
                    "Délivrance des prises en charge"
                ),
                tabItemName = "Processus de traitement"
            )
        )

        var contentRente = mutableListOf<Data>(
            Data(
                title = "Rente",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Déclaration d’accident du travail")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat médical initial et final"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "PV de la police"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Rapport d’expertise médicale"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Procès-verbal d’enquête"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Le relevé de salaire (12 mois précédant l’accident)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extrait d’acte de naissance de la victime"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Photographie CNI"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Deux photos d’identité")
                )
            ),
            Data(
                isActive=false
            )
        )

        var contentRenteSurvivants = mutableListOf<Data>(
            Data(
                title = "Rente survivants",
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Déclaration d’accident du travail")
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extraits d’acte de naissance des enfants"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extrait d’acte de mariage"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extrait d’acte de décès"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat de non divorce, de non remariage et de non séparation de corps"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Ordonance de tutorat en cas de non mariage (désignation du ou des tuteurs des enfants) ou rapport de l’assistance sociale"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificats de scolarité ou d’apprentissage (jusqu’à 21 ans)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificats de visites périodiques pour les enfants de moins de 6 ans"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Certificat de vie et de charge des enfants"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Photographie carte d’identité du ou des tuteurs (ne plus produire si la conjointe est désignée tutrice de ses enfants)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Deux photos du ou des tuteurs (ne plus produire si la conjointe est désignée tutrice de ses enfants)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extraits d’acte de naissance (ascendants)"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Extraits d’acte de décès"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Rapport d’enquête de l’inspection du travail ou d’un centre social de la localité attestant que les ascendants étaient à la charge de la victime")
                )
            ),
            Data(
                isActive=false
            )
        )


        private var isAccidentTravail = true;
    }


}