package co.opensi.cnss.ui.assuresbeneficiaires.simulation

import android.opengl.Visibility
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import co.opensi.cnss.utils.NumericFormatter
import kotlinx.android.synthetic.main.fragment_simulation_salaire_estimation.*
import kotlinx.android.synthetic.main.fragment_simulation_salaire_estimation.view.*
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimulationSalaireEstimationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SimulationSalaireEstimationFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private val dateFormatter =
        NumericFormatter(stringPattern = "xx/xxxx", charNumberRepresentation = 'x')

    private var period1 = 0
    private var period2 = 0
    private var period3 = 0
    private var totalPeriod = 0

    private var periodThirdZoneArea : View?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(
            R.layout.fragment_simulation_salaire_estimation,
            container,
            false
        )

        addPrecedenceInPeriod(root.firstPeriodFrom, root.secondPeriodFrom, 1)
        addPrecedenceInPeriod(root.secondPeriodFrom,root.thirdPeriodFrom, 0, true)//ca cest juste un check
        addPrecedenceInPeriod(root.thirdPeriodFrom, root.fourfthPeriodFrom, 2)
        addPrecedenceInPeriod(root.fourfthPeriodFrom,root.fifthPeriodFrom, 0, true)//ca cest juste un check
        addPrecedenceInPeriod(root.fifthPeriodFrom, root.sixthPeriodFrom, 3)

        periodThirdZoneArea = root.periodThirdZone

        (requireActivity()as HomeActivity).setActionBarTitle("Simulation")
        Functions.setBoldTextView(root.topText, requireContext(), true)

        root.radioNoLayout.visibility = View.GONE
        root.radioYesLayout.visibility = View.GONE

        root.validate.setOnClickListener {
            Functions.hideKeyboard(requireActivity(), requireView())
            if (Functions.checkEmptyFields(root.salaryET)){
                requireView().findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "salary",
                    root.salaryET.text.toString()
                )
                requireView().findNavController().popBackStack()
            }
        }

        root.validateSecond.setOnClickListener {
            Functions.hideKeyboard(requireActivity(), requireView())
            //on doit rapatrier le montant
            // normalement cest le meme traitrement que ci haut
            Functions.hideKeyboard(requireActivity(), requireView())

            //on doit verifier que aucun des champs de date na derreur si erreur on ne doit pas valider le calcul
            if (checkFields()) {
                var salaryCompute = 0.0

                //si pas derreur, on doit calculer le salaire moyen et le retourner a la vue d'avant
                if(!periodThirdZone.isVisible){
                    salaryCompute = ( (root.salaryFirst.text.toString().toDouble()*period1) + (root.salarySecond.text.toString().toDouble()*period2))
                        .div( (period1+period2) )
                }else{
                    salaryCompute = ( (root.salaryFirst.text.toString().toDouble()*period1) + (root.salarySecond.text.toString().toDouble()*period2) + (root.salaryThird.text.toString().toDouble()*period3))
                        .div( (period1+period2+period3) )
                }

                salaryCompute = Functions.roundDecimal(salaryCompute)!!

                requireView().findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    "salary",
                    salaryCompute.toString()
                )
                requireView().findNavController().popBackStack()
            }


        }

        root.radioYesNo!!.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radioNo) {
                //updateNow = false
                //on lui demande le montant du salaire
                root.radioNoLayout.visibility = View.VISIBLE
                root.radioYesLayout.visibility = View.GONE
            } else {
                //updateNow = true
                //on lui montre les périodes de simulation
                root.radioNoLayout.visibility = View.GONE
                root.radioYesLayout.visibility = View.VISIBLE
            }
        }

        return root
    }

    private fun addPrecedenceInPeriod(
        firstPeriodFrom: EditText,
        secondPeriodFrom: EditText,
        period: Int,
        checkAnteriority: Boolean= false
    ) {
        secondPeriodFrom.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(view: View?, hasFocus: Boolean) {
                if (!secondPeriodFrom.text.toString().trim().isEmpty()) {
                    //on fait la verif pour voir si la date passée est compatible
                    val firstDate = firstPeriodFrom.text.toString().trim().split("/")
                    val secondDate = secondPeriodFrom.text.toString().trim().split("/")

                    val dateFormatter = SimpleDateFormat("dd/MM/yyyy")
                    val dateFirst = dateFormatter.parse("01/${firstDate[0]}/${firstDate[1]}")
                    val dateSecond = dateFormatter.parse("01/${secondDate[0]}/${secondDate[1]}")

                    if (!dateSecond.after(dateFirst)) {
                        secondPeriodFrom.setError("Date de fin de période incorrecte")
                        return
                    }

                    if(checkAnteriority){
                        return
                    }


                    val startCalendar: Calendar = GregorianCalendar()
                    startCalendar.setTime(dateFirst)
                    val endCalendar: Calendar = GregorianCalendar()
                    endCalendar.setTime(dateSecond)

                    val diffYear: Int =
                        endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR)
                    val diffMonth: Int =
                        diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH)

                    //Toast.makeText(requireContext(),"La durée en mois est de ${diffMonth}",Toast.LENGTH_SHORT).show()



                    when(period){
                        1-> {
                            period1 = diffMonth
                            hintFirstPeriod.text = "Période 1 ( ${period1} mois)"
                        }
                        2-> {
                            period2 = diffMonth
                            hintSecondPeriod.text = "Période 2 ( ${period2} mois)"
                        }
                        3-> {
                            period3 = diffMonth
                            hintThirdPeriod.text = "Période 3 ( ${period3} mois)"
                        }
                    }

                    totalPeriod=period1+period2+period3
                    Toast.makeText(requireContext(),"La durée totale est de ${totalPeriod} mois",Toast.LENGTH_SHORT).show()

                    if(period == 2){
                        if(totalPeriod<60){
                            periodThirdZone!!.visibility = View.VISIBLE
                            warningSixty.visibility = View.GONE
                        }else{
                            if (totalPeriod==60){
                                //traitement pour afficher le message flash
                                warningSixty.visibility = View.VISIBLE
                                warningText.text = "Vous avez atteint les 60 mois réquis en " +
                                        "${getResources().getStringArray(R.array.months)[secondPeriodFrom.text.split("/")[0].toInt()-1]   }/${secondPeriodFrom.text.split("/")[1]}"
                            }
                            periodThirdZone!!.visibility = View.GONE
                            //Quand ceci est bon, on doit supprimer toutes les erreurs qu'il y a sur les champs du 3e formulaire si éventuellement il y en a
                            //meme sil est gone les erreurs demeurent
                        }
                    }else if (period ==3){
                        //meme traitement que ligne 183
                        warningSixty.visibility = View.VISIBLE
                        if(totalPeriod<60){
                            warningSixty.visibility = View.GONE
                            //peut etre qu'il faut mettre un message derreur pour dire que les 60 mois ne sont pas atteints
                        }else if (totalPeriod==60){
                            //traitement pour afficher le message flash
                            warningSixty.visibility = View.VISIBLE
                            warningText.text = "Vous avez atteint les 60 mois réquis en " +
                                    "${getResources().getStringArray(R.array.months)[secondPeriodFrom.text.split("/")[0].toInt()-1]   }/${secondPeriodFrom.text.split("/")[1]}"
                        }
                    }


                    if (totalPeriod>60){
                        secondPeriodFrom.setError("Vous avez dépassé les 60 mois")
                        return
                    }

                }
            }
        })
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val monthYearTextWatcher = object : TextWatcher {
            private var updatedText: String = ""
            private var editing: Boolean = false

            override fun afterTextChanged(editable: Editable?) {
                if (editing) return

                editing = true
                editable?.run {
                    clear()
                    insert(0, updatedText)
                }
                editing = false
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                val input = dateFormatter(text?.toString().orEmpty())
                if (input.contentEquals(updatedText) || editing) return
                updatedText = input
            }
        }

        firstPeriodFrom.addTextChangedListener(monthYearTextWatcher)
        secondPeriodFrom.addTextChangedListener(monthYearTextWatcher)
        thirdPeriodFrom.addTextChangedListener(monthYearTextWatcher)
        fourfthPeriodFrom.addTextChangedListener(monthYearTextWatcher)
        fifthPeriodFrom.addTextChangedListener(monthYearTextWatcher)
        sixthPeriodFrom.addTextChangedListener(monthYearTextWatcher)
    }


    fun checkFields(): Boolean {
        var verifPeriods = true
        var verifEmptyness = true
        var verifSalaries = true

        if (!periodThirdZone.isVisible){// dans ce cas on ne suppose que 2 périodes
            verifSalaries = Functions.checkEmptyFields(salaryFirst) && Functions.checkEmptyFields(salarySecond)
            if (firstPeriodFrom.error != null || secondPeriodFrom.error != null || thirdPeriodFrom.error != null
                || fourfthPeriodFrom.error != null ) {
                verifPeriods = false
            }

            verifEmptyness = Functions.checkEmptyFields(firstPeriodFrom) && Functions.checkEmptyFields(secondPeriodFrom)
                    && Functions.checkEmptyFields(thirdPeriodFrom) && Functions.checkEmptyFields(fourfthPeriodFrom)

        }else{// dans ce cas on suppose 3 périodes
            verifSalaries = Functions.checkEmptyFields(salaryFirst) && Functions.checkEmptyFields(salarySecond) && Functions.checkEmptyFields(salaryThird)
            if (firstPeriodFrom.error != null || secondPeriodFrom.error != null || thirdPeriodFrom.error != null
                || fourfthPeriodFrom.error != null || fifthPeriodFrom.error != null || sixthPeriodFrom.error != null) {
                verifPeriods = false
            }

            verifEmptyness = Functions.checkEmptyFields(firstPeriodFrom) && Functions.checkEmptyFields(secondPeriodFrom) && Functions.checkEmptyFields(thirdPeriodFrom)
                    && Functions.checkEmptyFields(fourfthPeriodFrom) && Functions.checkEmptyFields(fifthPeriodFrom) && Functions.checkEmptyFields(sixthPeriodFrom)
        }

        return verifPeriods && verifEmptyness && verifSalaries
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Functions.hideKeyboard(requireActivity(), requireView())
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimulationSalaireEstimationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SimulationSalaireEstimationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}