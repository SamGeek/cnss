package co.opensi.cnss.ui.assuresbeneficiaires.prestations

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.Prestation
import co.opensi.cnss.ui.assuresbeneficiaires.assurancevieillesse.AssuranceVieillesseFragment
import co.opensi.cnss.ui.assuresbeneficiaires.prestationsfamiliales.PrestationsFamilialesFragment
import co.opensi.cnss.ui.assuresbeneficiaires.risquespro.accidenttravail.AccidentTravailFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions.Companion.afterTextChanged
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.*
import kotlinx.android.synthetic.main.fragment_prestations.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PrestationsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PrestationsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var prestationAdapter: PrestationsAdapter? = null
    private var prestationsFiltered = mutableListOf<Prestation>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        (requireActivity() as HomeActivity).setActionBarTitle("My CNSS")
        (requireActivity() as HomeActivity).hideToolBar(true)

        val rootView = inflater.inflate(R.layout.fragment_prestations, container, false)
        //retrieve the searchView and


        rootView.backContainer.setOnClickListener {
            (requireActivity() as HomeActivity).hideToolBar()
            requireView().findNavController().popBackStack()
        }

        rootView.searchText.afterTextChanged {
            //Toast.makeText(requireContext(),it+" length "+it.length ,Toast.LENGTH_LONG).show()

            if(it.length==0){
                //on affiche  la liste initiale
                //ceci est utile quand on supprime progressivement tout le contenu du champ de recherche juska obtenir une liste vide
                prestationAdapter!!.setItems(prestations)
            }else{
                //on filtre la liste avec le pattern envoyé
                val result  = prestations.toMutableList().filter { prestation -> prestation.libelle.contains(it, true) }
                prestationsFiltered  = result.toMutableList()
                prestationAdapter!!.setItems(prestationsFiltered)
                //gérer l'empty state quand aucun service ne corresponds a la recherche
            }

        }

        prestationAdapter = PrestationsAdapter(
            prestations,
            activity as AppCompatActivity,
            object : PrestationsAdapter.Listener {
                override fun onPrestationChoosen(prestation: Prestation) {
                    //9,7,18,10,11,22,19 ,20,21,8 //normalement le 8 aussi je pense
                    //17,16,13,4,15,14,3,2,1
                    //6,5,12,0

                   // Toast.makeText(requireContext(),"Click",Toast.LENGTH_LONG).show()

                    var data = mutableListOf<Data>()

                    if(prestation.id in mutableListOf<Int>(9,7,18,10,11,22,19 ,20,21,8)){
                        ////ce sont les       RISQUES PRO / MALADIES PRO

                        when(prestation.id){
                            9-> data = AccidentTravailFragment.contentDeclarationMaladieProfessionnelle
                            7-> data = AccidentTravailFragment.contentContinuiteSoins
                            18-> data = AccidentTravailFragment.contentReglementFactures
                            10->  data = AccidentTravailFragment.contentDemandeRemboursements
                            11-> data = AccidentTravailFragment.contentIndemnitesJournaliers
                            22-> data = AccidentTravailFragment.contentTraitementRechutes
                            19->  data = AccidentTravailFragment.contentRemplacementProthese
                            20->  data = AccidentTravailFragment.contentRente
                            21->  data = AccidentTravailFragment.contentRenteSurvivants
                            8->  data = AccidentTravailFragment.contentAccidentTravail
                        }

                        requireView().findNavController().navigate(R.id.action_prestationsFragment_to_pensionDetailsFragment,
                            bundleOf("content" to data)
                        )

                    }else if(prestation.id in mutableListOf<Int>(17,16,13,4,15,14,3,2,1 )){
                        //ce sont les        ASSURANCE VIELLESSE / Details
                        when(prestation.id){
                            17-> data = AssuranceVieillesseFragment.contentPensionVieillesseNoramle
                            16-> data = AssuranceVieillesseFragment.contentPensionVieillesseAnticipée
                            13-> data = AssuranceVieillesseFragment.contentPensionInvalidite
                            4->  data = AssuranceVieillesseFragment.contentAllocationVieillesse
                            15-> data = AssuranceVieillesseFragment.contentPensionVeuf
                            14-> data = AssuranceVieillesseFragment.contentPensionSurvivantOrphelin
                            3->  data = AssuranceVieillesseFragment.contentAllocationSurvivantAscendant// ce nest pas le bon
                            2->  data = AssuranceVieillesseFragment.contentAllocationSurvivantOrphelin
                            1->  data = AssuranceVieillesseFragment.contentAllocationSurvivantAscendant
                        }
                        requireView().findNavController().navigate(R.id.action_prestationsFragment_to_pensionDetailsFragment,
                            bundleOf("content" to data)
                        )

                    }else {
                        //6,5,12,0
                        //// Ce sont les DEMANDE DE PRESTATION
                        when(prestation.id){
                            6-> data = PrestationsFamilialesFragment.contentAllocationsPrenatales
                            5-> data = PrestationsFamilialesFragment.contentAllocationsFamiliales
                            12-> data = PrestationsFamilialesFragment.contentIndemnitesCongesMaternite
                            0->  data = PrestationsFamilialesFragment.contentActionSanitaireSociale
                        }
                        requireView().findNavController().navigate(R.id.action_prestationsFragment_to_pensionDetailsFragment,
                            bundleOf("content" to data)
                        )
                    }

                }
            }
        )

        val ccLayoutManager = GridLayoutManager(context, 3)
        rootView.recycler_choose_prestation.apply {
            layoutManager = ccLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = prestationAdapter
        }

        return rootView
    }

    companion object {

        //il faut aussi gerer la destination lorsqu'on clique sur certaines des catégories
        //update les prestations later avec libeles et icones
        val prestations = mutableListOf<Prestation>(
            Prestation(0,R.drawable.ic_action_sanitaire_sociale,"Action Sanitaire et sociale"),//to find
            Prestation(1,R.drawable.ic_allocation_survivant_ascendant,"allocation de survivant ascendant"),//to find
            Prestation(2,R.drawable.ic_allocation_survivant_orphelin,"Allocation de survivant orphelins"),
            Prestation(3,R.drawable.ic_pension_survivant_veuf,"Allocation de survivant veuve"),
            Prestation(4,R.drawable.ic_pension_vieillesse_normale,"Allocation de vieillesse"),
            Prestation(5,R.drawable.ic_allocations_familiales,"Allocations famililales"),//to find
            Prestation(6,R.drawable.ic_allocations_prenatales,"Allocations prénatales"),//to find
            Prestation(7,R.drawable.ic_continuite_soins,"Continuité des soins"),
            Prestation(8,R.drawable.ic_accident_travail,"Déclaration d'accident de travail"),
            Prestation(9,R.drawable.ic_maladies_pro,"Déclaration de maladies profefesionnelles"),
            Prestation(10,R.drawable.ic_demande_remboursements,"Demande de remboursements"),//to fix
            Prestation(11,R.drawable.ic_indemnite_journaliere,"Indemnité journalière"),
            Prestation(12,R.drawable.ic_indemnites_conges_maternite,"Indemnités de congés de maternité"),//to find
            Prestation(13,R.drawable.ic_pension_invalidite,"Pension d’invalidité"),
            Prestation(14,R.drawable.ic_allocation_survivant_orphelin,"Pension de survivant orphelin"),
            Prestation(15,R.drawable.ic_pension_survivant_veuf,"Pension de survivant veuve"),
            Prestation(16,R.drawable.ic_pension_vieillesse_anticipee,"Pension de vieillesse anticipée"),
            Prestation(17,R.drawable.ic_pension_vieillesse_normale,"Pension de vieillesse normale"),
            Prestation(18,R.drawable.ic_reglement_facture,"Règlement des factures"),
            Prestation(19,R.drawable.ic_remplacement_prothese,"Remplacement de prothèse"),
            Prestation(20,R.drawable.ic_rente,"Rente"),
            Prestation(21,R.drawable.ic_rente,"Rente survivants"),
            Prestation(22,R.drawable.ic_traitement_rechutes,"Traitement des rechutes")
        )


        val prestationsOthers = mutableListOf<Prestation>(

            //ce sont les       RISQUES PRO / MALADIES PRO
            Prestation(9,R.drawable.ic_maladies_pro,"Déclaration de maladies profefesionnelles"),//to find
            Prestation(7,R.drawable.ic_continuite_soins,"Continuité des soins"),
            Prestation(18,R.drawable.ic_reglement_facture,"Règlement des factures"),
            Prestation(10,R.drawable.ic_demande_remboursements,"Demande de remboursements"),//to fix
            Prestation(11,R.drawable.ic_indemnite_journaliere,"Indemnité journalière"),
            Prestation(22,R.drawable.ic_traitement_rechutes,"Traitement des rechutes"),
            Prestation(19,R.drawable.ic_remplacement_prothese,"Remplacement de prothèse"),
            Prestation(20,R.drawable.ic_rente,"Rente"),
            Prestation(21,R.drawable.ic_rente,"Rente survivants"),


            //9,7,18,10,11,22,19 ,20,21,8 //normalement le 8 aussi je pense
            //17,16,13,4,15,14,3,2,1
            //6,5,12,0



            //ce sont les       RISQUES PRO / ACCIDENT DE TRAVAIL          //il est le seul dans sa categorie
            //mais est redirigé au meme endroit que ci dessus
            Prestation(8,R.drawable.ic_accident_travail,"Déclaration d'accident de travail"),


            //ce sont les        ASSURANCE VIELLESSE / Details
            Prestation(17,R.drawable.ic_pension_vieillesse_normale,"Pension de vieillesse normale"),
            Prestation(16,R.drawable.ic_pension_vieillesse_anticipee,"Pension de vieillesse anticipée"),
            Prestation(13,R.drawable.ic_pension_invalidite,"Pension d’invalidité"),
            Prestation(4,R.drawable.ic_pension_vieillesse_normale,"Allocation de vieillesse"),
            Prestation(15,R.drawable.ic_pension_survivant_veuf,"Pension de survivant veuve"),//to find
            Prestation(14,R.drawable.ic_allocation_survivant_orphelin,"Pension de survivant orphelin"),
            Prestation(3,R.drawable.ic_pension_survivant_veuf,"Allocation de survivant veuve"),//to find
            Prestation(2,R.drawable.ic_allocation_survivant_orphelin,"Allocation de survivant orphelins"),
            Prestation(1,R.drawable.ic_allocation_survivant_ascendant,"allocation de survivant ascendant"),

            //17,16,13,4,15,14,3,2,1


            // Ce sont les DEMANDE DE PRESTATION
            Prestation(6,R.drawable.ic_allocations_prenatales,"Allocations prénatales"),
            Prestation(5,R.drawable.ic_allocations_familiales,"Allocations famililales"),//to find
            Prestation(12,R.drawable.ic_indemnites_conges_maternite,"Indemnités de congés de maternité"),
            Prestation(0,R.drawable.ic_action_sanitaire_sociale,"Action Sanitaire et sociale")//to find

            //6,5,12,0

        )
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PrestationsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PrestationsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}