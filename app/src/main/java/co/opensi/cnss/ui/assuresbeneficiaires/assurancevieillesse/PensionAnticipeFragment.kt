package co.opensi.cnss.ui.assuresbeneficiaires.assurancevieillesse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.opensi.cnss.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_pension_details.view.*
import kotlinx.android.synthetic.main.fragment_pieces_assurance_vieillesse.view.*

class PensionAnticipeFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val rootView  = inflater.inflate(R.layout.fragment_pension_details, viewGroup, false)
//        requireActivity().setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)

        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter

        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))



        return rootView
    }



    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ConditionsAssuranceVieillesseFragment (defined as a static inner class below).
            lateinit var fragment : Fragment

            if (position == 0){
                fragment  =
                    ConditionsAssuranceVieillesseFragment.newInstance()

            }else if( position == 2){
                fragment =
                    DelaiPrescriptionAssuranceVieillesseFragment.newInstance()
            }else{
                fragment =
                    PiecesAssuranceVieillesseFragment.newInstance()
            }

            fragment.retainInstance = true
            return  fragment

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 3
        }
    }



    /**
     * A placeholder fragment containing a custom view now.
     */
    class ConditionsAssuranceVieillesseFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_conditions_assurance_vieillesse,
                container,
                false
            )
            rootView.firstLabel.text= "Tout assuré qui a atteint au moins l’âge de 55 \nans peut demander la jouissance d’une \npension de vieillesse anticipée s’il remplit les \nconditions suivantes :"

            return rootView
        }

        companion object {
            fun newInstance(): ConditionsAssuranceVieillesseFragment {
                val fragment =
                    ConditionsAssuranceVieillesseFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

    class DelaiPrescriptionAssuranceVieillesseFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_delai_prescription_assurance_vieillesse,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): DelaiPrescriptionAssuranceVieillesseFragment {
                val fragment =
                    DelaiPrescriptionAssuranceVieillesseFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }


    class PiecesAssuranceVieillesseFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_pieces_assurance_vieillesse,
                container,
                false
            )

            rootView.firstLabel.text= "Constituer un dossier de pension anticipée \ncomprenant les pièces suivantes :"

            return rootView
        }

        companion object {
            fun newInstance(): PiecesAssuranceVieillesseFragment {
                val fragment =
                    PiecesAssuranceVieillesseFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

}