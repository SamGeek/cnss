package co.opensi.cnss.ui.employeurs.recouvrement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_pension_details.view.*
import kotlinx.android.synthetic.main.fragment_simple_list_content.view.*
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.*

class RecouvrementItemDetailsFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var content = mutableListOf<Data>()
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView  = inflater.inflate(R.layout.fragment_recouvrement_details, viewGroup, false)
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        content =  arguments?.getParcelableArrayList<Data>("content")!!

        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter
        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))
        tabLayout = rootView.tabs
        updateTabsState()

        return rootView
    }

    private fun updateTabsState() {
        for (i in 0..mSectionsPagerAdapter!!.count-1) {
            val data = content.get(i)
            if (!data.isActive){
                tabLayout.getTabAt(i)!!.view.visibility = View.GONE
            }else{
                tabLayout.getTabAt(i)!!.setText(data.tabItemName)
            }
        }
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ConditionsAssuranceVieillesseFragment (defined as a static inner class below).
            var fragment : Fragment =   retrieveAppropriateFragment(position)
            fragment.retainInstance = true

            return  fragment
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }
    }

    private fun retrieveAppropriateFragment(position : Int) : Fragment {
        var data = content.get(position)
        var fragment: Fragment  = SimpleTextFragment.newInstance(data)

        if (!data.isActive){
            //si le fragement ne doit pas etre activé, on le masque juste et pi cest fini
            //trouver comment le masquer ici
            tabLayout.getTabAt(position)!!.view.visibility = View.GONE

        }else {
            tabLayout.getTabAt(position)!!.text = data.tabItemName

            //tout le traitement se fait ici
            //pour eviter de faire des choses si le fragment ne doit pas etre actif
            if (data.isTextContent){
                //sil sagit juste d'afficher du text, on  affiche ce texte dans un fragment qui affiche du contenu texte
                //on appelle bon fragment et on lui passe le texte a afficher
                fragment = SimpleTextFragment.newInstance(data)
            }else if (data.isListContent){
                //la on doit afficher le bon fragement avec une liste de choses
                val array = arrayListOf<TextIcon>()
                array.addAll(data.listContent)
                fragment  = SimpleListContentFragment.newInstance(data)
            }
        }

        return fragment
    }

    class SimpleTextFragment : Fragment() {
        private var listItemsAdapter: SimpleTextItemsAdapter? = null
        private var textContent: String? = null
        private var data = Data()
        private var dotListData = mutableListOf<String>()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(textContentParam)!!
                textContent = data.textContent
                dotListData = data.dotListContent
            }
        }
        
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_simple_text_content,
                container,
                false
            )
            if (data.hideTextContent){
                rootView.textContentTV.visibility= View.GONE
            }else{
                rootView.textContentTV.visibility= View.VISIBLE
                //on mets a jour avec le bon texte
                rootView.textContentTV.text = textContent
            }


            //we will add here the functionnality to show TextList
            if (data.hasTextWithDotList){//only if it has dotList
                rootView.recycler_text_items.visibility = View.VISIBLE

                listItemsAdapter = SimpleTextItemsAdapter(
                    dotListData,
                    activity as AppCompatActivity,
                    object : SimpleTextItemsAdapter.Listener {
                        override fun onItemChoosen(textIcon: String) {
                        }
                    }
                )

                val ccLayoutManager = LinearLayoutManager(context)
                rootView.recycler_text_items.apply {
                    layoutManager = ccLayoutManager
                    itemAnimator = DefaultItemAnimator()
                    adapter = listItemsAdapter
                }
            }else{
                rootView.recycler_text_items.visibility = View.GONE
            }


            return rootView
        }

        companion object {
            private const val textContentParam = "textContentParam"
            fun newInstance(content: Data) =
                SimpleTextFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(textContentParam, content!!)
                    }
                }
        }
    }

    class SimpleListContentFragment() : Fragment() {
        private var listItemsAdapter: SimpleListItemsAdapter? = null
        private var listData = mutableListOf<TextIcon>()
        private var data = Data()


        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(SimpleListContentFragment.itemsParam)!!
                listData = data.listContent
            }
        }

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_simple_list_content,
                container,
                false
            )

            //ici on charge les données calmement
            //je vais essayer de serialiser les données en Json pour voir si ca marche sinon
            //opter pour une autre solution

            if (data.hasNotaBene){
                rootView.notaBene.visibility = View.VISIBLE
                rootView.notaBene.text = data.notaBeneContent
            }else {
                rootView.notaBene.visibility = View.GONE
            }

            listItemsAdapter = SimpleListItemsAdapter(
                listData,
                activity as AppCompatActivity,
                object : SimpleListItemsAdapter.Listener {
                    override fun onItemChoosen(textIcon: TextIcon) {
//                      Toast.makeText(requireContext(), "clicked", Toast.LENGTH_LONG).show()
                    }
                }
            )

            val ccLayoutManager = LinearLayoutManager(context)
            rootView.recycler_list_items.apply {
                layoutManager = ccLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = listItemsAdapter
            }

            return rootView
        }

        companion object {
            private const val itemsParam = "itemsParam"
            fun newInstance(content: Data) =
                SimpleListContentFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(itemsParam, content!! )
                    }
                }
        }
    }

}