package co.opensi.cnss.ui.assuresbeneficiaires.risquespro.accidenttravail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.ui.employeurs.recouvrement.RecouvrementFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_affiliation.view.*
import kotlinx.android.synthetic.main.fragment_immatriculation.view.*
import kotlinx.android.synthetic.main.fragment_immatriculation_affiliation.view.*

class ImmatriculationAffiliationFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView  = inflater.inflate(R.layout.fragment_immatriculation_affiliation, viewGroup, false)
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        (requireActivity()as HomeActivity).setActionBarTitle( "Immatriculation & affiliation")

        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter
        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))

        return rootView
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DefinitionFragment (defined as a static inner class below).
            lateinit var fragment : Fragment

            if (position == 0){
                fragment  =
                    ImmatriculationFragment.newInstance()
            }else{
                fragment =
                    AffiliationFragment.newInstance()
            }

            fragment.retainInstance = true
            return  fragment

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 2
        }
    }


    class ImmatriculationFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_immatriculation,
                container,
                false
            )

            //je dois faire une action pour naviguer dici vers pension details
            rootView.rl_immatriculation_employeur_regime_general.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_immatriculationAffiliationFragment_to_pensionDetailsFragment,
                    bundleOf("content" to RecouvrementFragment.contentImmatriculationRegimeGeneral))
            }


            rootView.rl_immatriculation_employeur_gens_maison.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_immatriculationAffiliationFragment_to_pensionDetailsFragment,
                    bundleOf("content" to RecouvrementFragment.contentImmatriculationGensMaison))
            }

            rootView.rl_immatriculation_assurance_volontaire.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_immatriculationAffiliationFragment_to_pensionDetailsFragment,
                    bundleOf("content" to RecouvrementFragment.contentImmatriculationAssuranceVolontaire))
            }

            return rootView
        }

        companion object {

            fun newInstance(): ImmatriculationFragment {
                val fragment =
                    ImmatriculationFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

    class AffiliationFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_affiliation,
                container,
                false
            )

            //je dois faire une action pour naviguer dici vers pension details
            rootView.rl_embauchage.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_immatriculationAffiliationFragment_to_pensionDetailsFragment,
                    bundleOf("content" to RecouvrementFragment.contentEmbauchage))
            }

            rootView.rl_debauchage.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_immatriculationAffiliationFragment_to_pensionDetailsFragment,
                    bundleOf("content" to RecouvrementFragment.contentDebauchage))
            }

            return rootView
        }

        companion object {

            fun newInstance(): AffiliationFragment {
                val fragment =
                    AffiliationFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }
}