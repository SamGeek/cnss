package co.opensi.cnss.ui.assuresbeneficiaires.simulation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.SimulationData
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.rl_pension_vieillesse_anticipee
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.rl_pension_vieillesse_normale
import kotlinx.android.synthetic.main.fragment_simulation.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimulationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SimulationFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_simulation, container, false)

        (requireActivity()as HomeActivity).setActionBarTitle( "Simulation")

        root.rl_pension_vieillesse_normale.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                //bundleOf("option" to ""))
                bundleOf("content" to contentPensionVieillesseNoramle))
        }

        root.rl_pension_vieillesse_anticipee.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                bundleOf("content" to contentPensionVieillesseAnticipee))
        }


//----begin here
        root.rl_pension_survivant_conjoint.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                bundleOf("content" to contentPensionSurvivantConjoint))
        }


        root.rl_pension_survivant_enfant.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                bundleOf("content" to contentPensionSurvivantEnfant))
        }

        root.rl_allocation_survivant.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                bundleOf("content" to contentAllocationSurvivant))
        }

        root.rl_charge_sociale_embauche.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_simulationAllocViellesseFragment2,
                bundleOf("content" to contentChargeSocialeEmbauche))
        }
        root.rl_calcul_majoration_retard.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_simulationFragment_to_majorationRetardFragment,
                bundleOf("content" to contentCalculMajorationRetard))
        }


        return root



    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Functions.hideKeyboard(requireActivity(), requireView())
    }

    companion object {

        val PENSION_VIEILLESSE_NORMALE = "pension_vieillesse_normale"
        val PENSION_VIEILLESSE_ANTICIPEE = "pension_vieillesse_anticipee"
        val PENSION_SURVIVANT_CONJOINT = "pension_survivant_conjoint"
        val PENSION_SURVIVANT_ENFANT = "pension_survivant_enfant"
        val ALLOCATION_SURVIVANT = "allocation_survivant"
        val CHARGE_SOCIALE_EMBAUCHE= "charge_sociale_emauche"
        val CALCUL_MAJORATION_RETARD = "calcul_majoration_retard"

        var contentPensionVieillesseNoramle =
            SimulationData(
                title="Simulation",
                firstText = "Rémunération Moyenne Mensuelle (Fcfa)",
                secondText = "Nombre de mois d'assurance",
                thirdText = "",
                hasThirdText = false,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_pension_vieillesse_normale, texte = "Pension et Allocation de vieillesse"
                ),
                option=PENSION_VIEILLESSE_NORMALE)

        var contentPensionVieillesseAnticipee =
            SimulationData(
                title="Simulation",
                firstText = "Rémunération Moyenne Mensuelle (Fcfa)",
                secondText = "Nombre de mois d'assurance",
                thirdText = "",
                hasThirdText = false,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_pension_vieillesse_anticipee, texte = "Pension de vieillesse anticipée"
                ),
                option= PENSION_VIEILLESSE_ANTICIPEE)



        var contentPensionSurvivantConjoint =
            SimulationData(
                title="Simulation",
                firstText = "Pension de vieillesse du décédé (Fcfa)",
                secondText = "Nombre de conjoints du décédé",
                thirdText = "",
                hasThirdText = false,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_allocation_survivant_orphelin, texte = "Pension de survivants (cas des conjoints)"
                ),
                option= PENSION_SURVIVANT_CONJOINT)



        var contentPensionSurvivantEnfant =
            SimulationData(
                title="Simulation",
                firstText = "Pension de vieillesse du décédé (Fcfa)",
                secondText = "Nombre d'orphelins mineurs (âge < 21 ans)",
                thirdText = "Nombre d'orphelins pris en compte ",
                hasThirdText = true,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_allocation_survivant_orphelin, texte = "Pension de survivants (cas des enfants)"
                ),
                option= PENSION_SURVIVANT_ENFANT)




        var contentAllocationSurvivant =
            SimulationData(
                title="Simulation",
                firstText = "Rémunération Moyenne Mensuelle (Fcfa)",
                secondText = "Nombre de mois d'assurance",
                thirdText = "",
                hasThirdText = false,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_allocation_survivant_orphelin, texte = "Allocation de survivants"
                ),
                option= ALLOCATION_SURVIVANT)

        //pour ceci, il faut gerer le risque en pourcentage, et son affichage dans linterface
        //ainsi que la recupération des valeurs choisies par l'utilisateur
        //il doit avoir aussi un custom succes message, propre a lui
        var contentChargeSocialeEmbauche =
            SimulationData(
                title="Simulation",
                firstText = "Taux de risque du secteur d'activité",
                secondText = "Salaire brut",
                thirdText = "",
                hasThirdText = false,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_prestations_accident_maladies, texte = "Charges sociales liées à l'embauche"
                ),
                option= CHARGE_SOCIALE_EMBAUCHE)



        //comportement plutot complexe, à gerer a part
        var contentCalculMajorationRetard =
            SimulationData(
                title="Simulation",
                firstText = "Mois au cours duquel retard a commencé",
                secondText = "Cotisation mensuelle (Fcfa)",
                thirdText = "Mois au cours duquel vous souhaiter payer",
                hasThirdText = true,
                headerTextIcon = TextIcon(
                    icon = R.drawable.ic_prestations_accident_maladies, texte = "Calcul de la majoration de retard"
                ),
                option= CALCUL_MAJORATION_RETARD)









        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimulationFragment.
         */
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimulationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SimulationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}