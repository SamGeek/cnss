package co.opensi.cnss.ui.employeurs.recouvrement

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import co.opensi.cnss.data.models.TextIcon


class SimpleListItemsAdapter(
    private var items: MutableList<TextIcon>?,
    var mActivity: AppCompatActivity,
    var listener: Listener
) : RecyclerView.Adapter<SimpleListItemsAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) :
        RecyclerView.ViewHolder(view) {
        var itemImage: ImageView
        var itemLabel: TextView
        var separator: View

        init {
            itemImage = view.findViewById<View>(R.id.image) as ImageView
            itemLabel = view.findViewById<View>(R.id.textContentTV) as TextView
            separator = view.findViewById<View>(R.id.separator) as View
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_simple_list_item, parent, false)

        return MyViewHolder(mActivity, itemView)
    }

    interface Listener {
        fun onItemChoosen(textIcon: TextIcon)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val prestation = items!![position] as TextIcon
        holder.itemImage.setBackgroundResource(prestation.icon)
        holder.itemLabel.text = prestation.texte

        if (position==items!!.size-1){
            holder.separator.visibility = View.INVISIBLE
        }

    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    fun setItems(data: MutableList<TextIcon>?): Unit {
        this.items = data
        notifyDataSetChanged()
    }

  
    fun addItem(item: TextIcon): Unit {
        this.items!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = "SimpleListItemsAdapter"
    }

}
