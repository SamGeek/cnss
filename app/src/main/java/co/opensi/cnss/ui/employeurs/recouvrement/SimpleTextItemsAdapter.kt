package co.opensi.cnss.ui.employeurs.recouvrement

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import kotlinx.android.synthetic.main.fragment_simple_text_item.view.*


class SimpleTextItemsAdapter(
    private var items: MutableList<String>?,
    var mActivity: AppCompatActivity,
    var listener: Listener,
    var showDot: Boolean = true
) : RecyclerView.Adapter<SimpleTextItemsAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) :
        RecyclerView.ViewHolder(view) {
        var itemLabel: TextView

        init {
            itemLabel = view.findViewById<View>(R.id.textContentTV) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_simple_text_item, parent, false)

        if (!showDot) {
            itemView.iv_step1.visibility = View.GONE
        }

        return MyViewHolder(mActivity, itemView)
    }

    interface Listener {
        fun onItemChoosen(textIcon: String)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val prestation = items!![position]
        holder.itemLabel.text = prestation
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    fun setItems(data: MutableList<String>?): Unit {
        this.items = data
        notifyDataSetChanged()
    }

  
    fun addItem(item: String): Unit {
        this.items!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = "SimpleTextItemsAdapter"
    }

}
