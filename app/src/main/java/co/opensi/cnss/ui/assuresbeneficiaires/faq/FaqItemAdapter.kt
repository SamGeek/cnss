package co.opensi.cnss.ui.assuresbeneficiaires.faq

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import co.opensi.cnss.data.models.FaqItem


class FaqItemAdapter(
    private var faqItems: MutableList<FaqItem>?,
    var mActivity: AppCompatActivity,
    var listener: Listener
) : RecyclerView.Adapter<FaqItemAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) :
        RecyclerView.ViewHolder(view) {
        var faqItemsLabel: TextView

        init {
            faqItemsLabel = view.findViewById<View>(R.id.textContentTV) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_simple_faq_item, parent, false)

        itemView.setOnClickListener {
//            Toast.makeText(mActivity,"toto", Toast.LENGTH_LONG).show()
        }

        return MyViewHolder(mActivity, itemView)
    }

    interface Listener {
        fun onFaqItemChoosen(faqItem: FaqItem)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val faqItem = faqItems!![position] as FaqItem
        holder.faqItemsLabel.text = faqItem.content

        holder.itemView.setOnClickListener {
            //a faire et bien
         //   Toast.makeText(mActivity,"Click", Toast.LENGTH_LONG).show()
            listener.onFaqItemChoosen(faqItems!!.get(position))
        }
    }

    override fun getItemCount(): Int {
        return faqItems!!.size
    }

    fun setItems(data: MutableList<FaqItem>?): Unit {
        this.faqItems = data
        notifyDataSetChanged()
    }

  
    fun addItem(item: FaqItem): Unit {
        this.faqItems!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = ""
    }

}
