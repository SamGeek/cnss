package co.opensi.cnss.ui.assuresbeneficiaires.agencesregionales

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.opensi.cnss.R
import co.opensi.cnss.ui.welcome.HomeActivity

class AdressesRegionalesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_adresses_regionales, container, false)


        (requireActivity()as HomeActivity).setActionBarTitle("Adresses des agences régionales")

//        root.assuranceVieillesse.setOnClickListener {
//            requireView().findNavController().navigate(R.id.action_nav_home_to_assuranceVieillesseFragment)
//        }




        return root
    }

}