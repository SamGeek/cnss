package co.opensi.cnss.ui.assuresbeneficiaires.simulation

import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.SimulationData
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import co.opensi.cnss.utils.Functions.Companion.hideKeyboard
import kotlinx.android.synthetic.main.fragment_simulation_allocation_vieillesse.*
import kotlinx.android.synthetic.main.fragment_simulation_allocation_vieillesse.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimulationAllocViellesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SimulationAllocViellesseFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var data: SimulationData? = null
    private var option: String? = null
    private var first: TextView? = null
    private var second: TextView? = null
    private var third: TextView? = null
    private var fourth: TextView? = null
    private var fifth: TextView? = null
    private var selected: TextView? = null
    private var items = mutableListOf<TextView?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(
            R.layout.fragment_simulation_allocation_vieillesse,
            container,
            false
        )

        data = arguments?.getParcelable<SimulationData>("content")
       // salary = arguments?.getString("salary")
        option = data!!.option


        (requireActivity()as HomeActivity).setActionBarTitle(data!!.title)

        root.headerIcon.setBackgroundResource(data!!.headerTextIcon!!.icon)
        root.headerText.text = data!!.headerTextIcon!!.texte



        root.firstTextTV.text = data!!.firstText
        root.secondTextTV.text = data!!.secondText
        //root.firstTextTV.text = data!!.firstText //gestion du third text

        if (option.equals(SimulationFragment.PENSION_VIEILLESSE_NORMALE) || option.equals(
                SimulationFragment.PENSION_VIEILLESSE_ANTICIPEE
            )
        ) {

            root.remuneration.setOnFocusChangeListener(object : View.OnFocusChangeListener {
                override fun onFocusChange(view: View?, hasFocus: Boolean) {
                    if (hasFocus) {
                        requireView().findNavController().navigate(
                            R.id.action_simulationAllocViellesseFragment2_to_simulationSalaireEstimationFragment
                        )
                    }
                }
            })

//            root.remuneration.setOnClickListener {
//                //i just need to launch the right fragment
//                requireView().findNavController().navigate(R.id.action_simulationAllocViellesseFragment2_to_simulationSalaireEstimationFragment)
//                //and retrieve  back the results // still pending
//            }

        }


        if (data!!.hasThirdText) {
            root.thirdContent.visibility = View.VISIBLE
            root.thirdTextTV.text = data!!.thirdText
        } else {
            root.thirdContent.visibility = View.GONE
        }


        if (option.equals(SimulationFragment.PENSION_SURVIVANT_CONJOINT) || option.equals(
                SimulationFragment.PENSION_SURVIVANT_ENFANT
            )
            || option.equals(SimulationFragment.PENSION_VIEILLESSE_ANTICIPEE)
        ) {
            root.firstTextSucces.visibility = View.GONE
        } else {
            root.firstTextSucces.visibility = View.VISIBLE
        }

        root.computeCalcul.setOnClickListener {
            //on naffiche le contenu de succes que si le montant du salaire est bien renseigné et on calcule le bon montant a faire afficher
            if(checkFields()){// les deux champs sont requis
                root.successContent.visibility = View.VISIBLE
                thirdTextSucces.text = "${Functions.roundDecimal(
                    root.remuneration.text.toString().toDouble() * 0.34
                )} Fcfa" //on change le texte
            }

            hideKeyboard(requireActivity(), requireView())
        }


        //si vieillesse anticipée, on doit afficher le bon truc
        if (option.equals(SimulationFragment.PENSION_VIEILLESSE_ANTICIPEE)) {
            root.chooseCurrentOption.visibility = View.VISIBLE
            //changer le titre de la section
            root.headerText.text = "Pension de vieillese anticipée"
            //gerer les changements de la valeurs

            first = root.firstTV
            second = root.secondTV
            third = root.thirdTV
            fourth = root.fourthTV
            fifth = root.fifthTV
            selected = second
            items = mutableListOf<TextView?>(first, second, third, fourth, fifth)

            for (i in 0..4) {
                items.get(i)!!.setOnClickListener {
                    selected = items.get(i)
                    updateOthers() // a optimiser
                    //Functions.toastLong(requireContext(), selected!!.text.toString())
                }
            }


        } else {
            root.chooseCurrentOption.visibility = View.GONE
        }



        if (option.equals(SimulationFragment.CHARGE_SOCIALE_EMBAUCHE)) {
            //on masque la premiere zone de texte
            root.firstAreaContent.visibility = View.GONE
            root.abbatementContainer.visibility = View.GONE

            //et ensuite on change le contenu de la zone custom de pourcentage
            root.chooseCurrentOption.visibility = View.VISIBLE
            root.firstTV.text = "1%"
            root.secondTV.text = "2%"
            root.thirdTV.text = "3%"
            root.fourthTV.text = "4%"
            root.fifthTV.text = "5%"

            first = root.firstTV
            second = root.secondTV
            third = root.thirdTV
            fourth = root.fourthTV
            fifth = root.fifthTV
            selected = second
            items = mutableListOf<TextView?>(first, second, third, fourth, fifth)

            for (i in 0..4) {
                items.get(i)!!.setOnClickListener {
                    selected = items.get(i)
                    updateOthers() // a optimiser
                    //Functions.toastLong(requireContext(), selected!!.text.toString())
                }
            }

            root.successContent.visibility = View.GONE

            root.computeCalcul.setOnClickListener {
                if(Functions.checkEmptyFields(dureeMois) ){
                    root.successEmbaucheContent.visibility = View.VISIBLE
                    hideKeyboard(requireActivity(), requireView())
                }

            }
        }
        return root
    }

    fun checkFields(): Boolean {
        return  Functions.checkEmptyFields(remuneration) && Functions.checkEmptyFields(dureeMois)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        if (option.equals(SimulationFragment.PENSION_VIEILLESSE_NORMALE) || option.equals(
//                SimulationFragment.PENSION_VIEILLESSE_ANTICIPEE
//            )
//        ) {
//
//            val layout= requireView()
//            view.remuneration.setOnTouchListener { view, motionEvent ->
//               Navigation.findNavController(layout)
//                    .navigate(R.id.action_simulationAllocViellesseFragment2_to_simulationSalaireEstimationFragment)
//                return@setOnTouchListener false
//            }
//        }

        val navController = requireView().findNavController()
        //a corriger absolument cest la meilleure maniere de faire
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("salary")?.observe(
            viewLifecycleOwner
        ) { result ->
            view.remuneration.setText(result)
        }

    }

    private fun updateOthers() {
        //ce traitement est rappele a chaque fois qu'on clique sur lun des 5 elements
        //penser apres a rajouter une petite animation

//        Functions.toastLong(requireContext(), "conversion 60 dp en px -> ${Functions.dpToPx(requireContext(), 60)}")
//        Functions.toastLong(requireContext(), "conversion 50 dp en px -> ${Functions.dpToPx(requireContext(), 50)}")

        //peut etre qu'il faut genere cette vue completement

        for (i in 0..4) {
            val current =  items.get(i)

            if (current == selected) {
                //on lui mets le custom bg


//                android:id="@+id/secondTV"
//                android:layout_width="60dp"
//                android:layout_height="60dp"
//                android:gravity="center"
//                android:text="58"
//                android:textSize="10sp"
//                android:layout_marginLeft="-10dp"
//                android:layout_marginRight="-10dp"

//                android:background="@drawable/border_blue_hover_round"
//                android:textColor="@color/surface_white"
//                android:elevation="5dp"


                val params: ViewGroup.LayoutParams = current!!.layoutParams
                params.height = Functions.dpToPx(requireContext(),55)
                params.width = Functions.dpToPx(requireContext(),55)
                current.layoutParams = params
                current.gravity = Gravity.CENTER
                current.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)

                val p = current.layoutParams as MarginLayoutParams
                p.setMargins(-10, 0, -10, 0)
                current.layoutParams = p

                current.setBackgroundResource(R.drawable.border_blue_hover_round)
                current.setTextColor(Color.parseColor("#FFFFFF"))
                ViewCompat.setElevation(current, 5.0f)


                //current!!.requestLayout()
            } else if (current == first) {
                //si non sil est pas celui qui est selectionné et que cest le premier
                //on lui mets son custom bg first


//                android:id="@+id/firstTV"
//                android:background="@drawable/border_empty_grey_round_left"
//                android:layout_width="60dp"
//                android:layout_height="50dp"
//                android:gravity="center"
//                android:text="59"
//                android:textColor="@color/surface_black"
//                android:textSize="10sp"

                val params: ViewGroup.LayoutParams = current!!.layoutParams
                params.width = Functions.dpToPx(requireContext(),55)
                params.height = Functions.dpToPx(requireContext(),45)
                current.layoutParams = params
                current.gravity = Gravity.CENTER
                current.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)

                val p = current.layoutParams as MarginLayoutParams
                p.setMargins(0, 0, 0, 0)
                current.layoutParams = p

                current.setTextColor(Color.parseColor("#000000"))
                current.setBackgroundResource(R.drawable.border_empty_grey_round_left)
                ViewCompat.setElevation(current, 0.0f)

               // first!!.requestLayout()
            } else if (current == fifth) {
                //si non sil est pas celui qui est selectionné et que cest le premier
                //on lui mets son custom bg first
                //si non sil est pas celui qui est selectionné et que cest le premier
                //on lui mets son custom bg first



//                android:id="@+id/fifthTV"
//                android:background="@drawable/border_empty_grey_round_right"
//                android:layout_width="60dp"
//                android:gravity="center"
//                android:layout_height="50dp"
//                android:text="55"
//                android:textColor="@color/surface_black"
//                android:textSize="10sp"

                val params: ViewGroup.LayoutParams = current!!.layoutParams
                params.width = Functions.dpToPx(requireContext(),55)
                params.height = Functions.dpToPx(requireContext(),45)
                current.layoutParams = params
                current.gravity = Gravity.CENTER
                current.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)

                val p = current.layoutParams as MarginLayoutParams
                p.setMargins(0, 0, 0, 0)
                current.layoutParams = p

                current.setTextColor(Color.parseColor("#000000"))
                current.setBackgroundResource(R.drawable.border_empty_grey_round_right)
                ViewCompat.setElevation(current, 0.0f)
                //first!!.requestLayout()
            } else {
                //si non cest un bg simple du coup
                //mettre aussi les autres parametrages


//                android:id="@+id/fourthTV"
//                android:background="@drawable/border_empty_grey_normal"
//                android:layout_width="60dp"
//                android:gravity="center"
//                android:layout_height="50dp"
//                android:text="56"
//                android:textColor="@color/surface_black"
//                android:textSize="10sp"

                val params: ViewGroup.LayoutParams = current!!.layoutParams
                params.width = Functions.dpToPx(requireContext(),55)
                params.height = Functions.dpToPx(requireContext(),45)
                current.layoutParams = params
                current.gravity = Gravity.CENTER
                current.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)

                val p = current.layoutParams as MarginLayoutParams
                p.setMargins(0, 0, 0, 0)
                current.layoutParams = p

                current.setTextColor(Color.parseColor("#000000"))
                current.setBackgroundResource(R.drawable.border_empty_grey_normal)
                ViewCompat.setElevation(current, 0.0f)
//                items.get(i)!!.requestLayout()
            }

        }
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimulationAllocViellesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SimulationAllocViellesseFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}