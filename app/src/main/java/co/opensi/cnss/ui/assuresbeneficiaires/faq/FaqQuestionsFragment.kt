package co.opensi.cnss.ui.assuresbeneficiaires.faq

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.FaqItem
import co.opensi.cnss.data.models.Prestation
import co.opensi.cnss.ui.assuresbeneficiaires.prestations.PrestationsAdapter
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_faq.view.*
import kotlinx.android.synthetic.main.fragment_faq.view.recycler_faq_questions
import kotlinx.android.synthetic.main.fragment_faq_questions.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FaqQuestionsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FaqQuestionsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var prestationAdapter: PrestationsAdapter? = null
    private var faqItemsAdapter: FaqItemAdapter? = null
    private var choosenPrestation: Prestation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_faq_questions, container, false)
        choosenPrestation = arguments?.getParcelable<Prestation>("content")
        rootView.contentTitle.text = choosenPrestation!!.libelle

        (requireActivity()as HomeActivity).setActionBarTitle("FAQ")


        //

        faqItemsAdapter = FaqItemAdapter(
            faqItems[choosenPrestation!!.id],//lui donner les bon elements
            activity as AppCompatActivity,
            object : FaqItemAdapter.Listener {
                override fun onFaqItemChoosen(faqItem: FaqItem) {
                    //apres ici aussi rajouter les bonnes données pour afficher le bon contenu
                    requireView().findNavController().navigate(R.id.action_faqQuestionsFragment_to_faqReponseFragment,
                        bundleOf("content" to faqItem))
                }
            }
        )

        val faqLayoutManager = LinearLayoutManager(context)
        rootView.recycler_faq_questions.apply {
            layoutManager = faqLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = faqItemsAdapter
        }

        return rootView
    }

    companion object {


        val faqItems = mutableListOf(

            mutableListOf<FaqItem>(//prestations familiales
                FaqItem(0,"Pourquoi les prestations familiales ?"),
                FaqItem(1,"Quelles sont les prestations familiales fournies par la CNSS ?"),
                FaqItem(2,"Quelles sont les conditions à remplir pour bénéficier des prestations familiales ?"),
                FaqItem(3,"Quelles sont les pièces à fournir pour bénéficier des prestations familiales ?"),
                FaqItem(4,"Quels sont les montants et conditions d’attribution des prestations familiales ?"),
                FaqItem(5,"Comment bénéficier des allocations familiales ?"),
                FaqItem(6,"Quels sont les pièces périodiques à fournir pour bénéficier des allocations familiales?"),
                FaqItem(7,"Quels sont les montants des allocations familiales ?"),
                FaqItem(8,"Quels sont les prestations en nature relative à l'action sanitaire et sociale ?")
            ),

            mutableListOf<FaqItem>(//pension de vieillesse
                FaqItem(9,"Quelles sont les prestations de la branche des pensions ?"),
                FaqItem(10,"Qu'est-ce qu'une pension de vieillesse ?"),
                FaqItem(11,"Qu'est-ce qu'une pensions d'invalidité ?"),
                FaqItem(12,"Qu'est-ce qu'une pension de survivants ?"),
                FaqItem(13,"Qu'est-ce qu'une allocation de vieillesse ?"),
                FaqItem(14,"Qu'est-ce qu'une allocation de survivants ?"),
                FaqItem(15,"Quel est le mode de calcul de la pension de vieillesse ?"),
                FaqItem(17,"Quel est le mode de calcul de la pension d'invalidité ?"),
                FaqItem(16,"Quel est le mode de calcul de l'allocation de vieillesse ?"),
                FaqItem(18,"Quel est le mode de calcul de la pension de survivants ?"),
                FaqItem(19,"Quelles sont les pièces à fournir pour beneficier de cest prestations ?")
            ),

            mutableListOf<FaqItem>(//Risques professionnels
                FaqItem(20,"Quels sont les objectifs de la branche des risques professionnels ?"),
                FaqItem(21,"Quelles sont les personnes protégées ?"),
                FaqItem(22,"Qu’est-ce qu’un accident de travail ?"),
                FaqItem(23,"Qu’est-ce qu’une maladie professionnelle ?"),
                FaqItem(24,"Quelles sont les formalités à remplir en cas d’accident du travail ou de maladie professionnelle ?"),
                FaqItem(25,"Quelles sont les autres obligations de la victime d’un accident de travail ?"),
                FaqItem(26,"Quelles sont les prestations servies par la branche des risques professionnels ?"),
                FaqItem(27,"Comment calculer la rente IPP ?"),
                FaqItem(28,"Comment constituer un dossier de  sinistre (indemnité journalière) ?"),
                FaqItem(29,"Comment constituer un dossier grave ?"),
                FaqItem(30,"Comment constituer un dossier des survivants et ascendants à charge ?"),
                FaqItem(31,"Quel est le délai pour réclamer vos droits ?")
            ),

            mutableListOf<FaqItem>(//Recouvrement des cotisations
                FaqItem(32,"Qu’est ce que l’immatriculation de l’employeur ?"),
                FaqItem(33,"Qu’est ce que l’affiliation des travailleurs ?"),
                FaqItem(34,"Comment sont prélevés les cotisations ?"),
                FaqItem(35,"Quels sont les taux de cotisations ?"),
                FaqItem(36,"Quels sont les pièces à produire lors du versement des cotisations ?"),
                FaqItem(37,"Quel est la périodicité de versement des cotisations ?"),
                FaqItem(38,"Qu’est ce qu’une majoration de retard ?\n"),
                FaqItem(39,"Quel sont les sanctions encourues en cas de non paiement des cotisations ?")
            ),

            mutableListOf<FaqItem>(//droit des travailleurs migrants
                FaqItem(40,"Qu’est ce qu’un travailleur migrant ?"),
                FaqItem(41,"Quels sont les pays pris en charge par le droit des travailleurs migrants ?"),
                FaqItem(42,"À quoi servent les instruments de coordination ?"),
                FaqItem(43,"Quelles sont les conditions à remplir pour bénéficier des droits des travailleurs migrants ?"),
                FaqItem(44,"Quels sont les droits des travailleurs migrants ?"),
                FaqItem(45,"Comment les droits des travailleurs migrants sont-ils liquidés et les prestations sont-elles payées ?")
            )


        )


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FaqQuestionsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FaqFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}