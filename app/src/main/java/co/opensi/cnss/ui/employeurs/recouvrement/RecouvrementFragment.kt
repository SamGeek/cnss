package co.opensi.cnss.ui.employeurs.recouvrement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.Section
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_recouvrement.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AssuranceVieillesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RecouvrementFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_recouvrement, container, false)

        (requireActivity()as HomeActivity).setActionBarTitle("Recouvrement des cotisations")


        //Il faut supprimer le RecouvrementItemDetailsFragment (supprimer les interfaces qui vont avec)
        //Vu que le Pension details fragement permets d'afficher tous les types de détails possibles


        //apres la mise a jour, plus que 2 menus a gerer

        root.taux_cotisation_periodicite.setOnClickListener {
            requireView().findNavController().navigate(
                R.id.action_recouvrementFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentTauxCotisationPeriodicite)
            )
        }


        root.declaration_cotisation.setOnClickListener {
            requireView().findNavController().navigate(
                R.id.action_recouvrementFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentDeclarationCotisationsNouveau)
            )
        }


//        root.immatriculation_employeur_regime_general.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentImmatriculationRegimeGeneral))
//        }
//
//        root.immatriculation_employeur_gens_maison.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentImmatriculationGensMaison))
//        }
//
//        root.immatriculation_assurance_volontaire.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentImmatriculationAssuranceVolontaire))
//        }
//
//        root.embauchage.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentEmbauchage))
//        }
//
//        root.debauchage.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentDebauchage))
//        }
//
//        //second part of menus
//
//        root.delivrance_immatriculation.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentDelivranceImmatriculation))
//        }
//
//        root.retrocession_cotisations.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentRetrocessionCotisations))
//        }
//
//        root.levee_prescription_assurance.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentLeveePrescriptionAssurance))
//        }
//
//        root.remise_majoration_retard.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentRemiseMajorationRetard))
//        }
//
//        root.declaration_cotisation.setOnClickListener{
//            requireView().findNavController().navigate(R.id.action_recouvrementFragment_to_recouvrementItemDetailsFragment,
//                bundleOf("content" to contentDeclarationCotisations))
//        }

        return root
    }

    companion object {

        //--------------------------------------
        //Nouveaux contenus modifiés

        var contentTauxCotisationPeriodicite = mutableListOf<Data>(
            Data(
                title = "Taux de cotisation et périodicité",
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                isActive=true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les cotisations sont versées au titre de chacune des branches du régime général de sécurité sociale géré par la CNSS."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les taux de cotisation sont :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Prestations familiales : 9% à la charge de l’employeur ;",
                            "Risques professionnels : 1 à 4% à la charge de l’employeur ; le taux des risques professionnels varie selon la nature de l’activité de l’entreprise ;",
                            "Pension : 10% dont :\n" +
                                    "- 6,4% à la charge de l’employeur ;\n" +
                                    "- 3,6% à la charge du travailleur.",
                            "L’allocation de survivants"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En ce qui concerne les apprentis et les élèves des écoles professionnelles, seule la cotisation des risques professionnels est versée."
                    )
                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Taux de cotisation"
            ),
            Data(
                isActive=true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le versement des cotisations est mensuel pour les employeurs occupant 20 salariés au moins. Le versement est trimestriel pour les employeurs occupant moins de 20 salariés. Les cotisations sont versées par l’employeur à la CNSS dans les 15 jours qui suivent la période pour laquelle elles sont dues."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En cas de cessation d’activité ou de cession de l’entreprise, les cotisations sont immédiatement exigibles."
                    )
                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Périodicité"
            ),
            Data(
                isActive=false
            )
        )

        var contentDeclarationCotisationsNouveau = mutableListOf<Data>(
            Data(
                title = "Déclaration de cotisations",
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                isActive=true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur est débiteur vis-à-vis de la Caisse de la cotisation totale y compris la part du salarié qui est précomptée sur la rémunération de celui-ci lors de chaque paie. Les cotisations sont portables et non quérables. L’employeur doit produire lors du versement des cotisations, les pièces justificatives suivantes :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "une déclaration de salaires et de cotisations ;",
                            "une déclaration nominative du personnel à fournir trimestriellement par les employeurs utilisant au moins vingt (20) salariés."
                        )
                    )
                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Versement des cotisations"
            ),
            Data(
                isActive=true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur qui ne verse pas les cotisations dans le délai prescrit est passible d’une majoration de 1.5% par mois ou fraction de mois de retard du montant des cotisations."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Cette majoration est due au même titre que la cotisation et payée dans les mêmes conditions."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur qui a contrevenu aux prescriptions légales en matière de sécurité sociale peut être poursuivi par la Caisse. Tout employeur débiteur des cotisations peut se voir opposer des saisies- arrêts pratiquées à la requête de la Caisse."
                    )
                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Retard et sanctions"
            ),
            Data(
                isActive=false
            )
        )

        //--------------------------------------

        var contentImmatriculationRegimeGeneral = mutableListOf<Data>(
            Data(
                title = "Immatriculation employeur régime général",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                tabItemName = "Description",
                textContent = "Tout employeur relevant du régime général de sécurité sociale est tenu de se faire immatriculer à la CNSS en vue de lui attribuer un numéro."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Demande d'immatriculation régime général"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "État de recensement du Personnel"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Avis d'embauchage ou état nominatif complémentaire"
                    )
                )
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                hasNotaBene = true,
                tabItemName = "Pièces à fournir",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Établissement unipersonnel : RC+IFU"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Société : RC+ statut* IFU"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "ONG ou Association : récépissé d'enregistrement + statut + règlement intérieur"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Établissement unipersonnel : RC+IFU"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Projet : accord de projet\""
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Ecole, centre de santé : Autorisation d'ouverture"
                    )
                ),
                notaBeneContent = "NB : copie de la facture SBEE ou SONEB du domicile abritant le siège."
            )
        )

        var contentImmatriculationGensMaison = mutableListOf<Data>(
            Data(
                title = "Immatriculation employeur de gens de maison",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                tabItemName = "Description",
                textContent = "Tout employeur de gens de maison est tenu de se faire immatriculer en vue d'avoir un numéro d'immatriculation."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Demande d’immatriculation d'un employeur de gens de maison"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "État de recensement"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Avis d'embauchage"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )

        var contentImmatriculationAssuranceVolontaire = mutableListOf<Data>(
            Data(
                title = "Immatriculation à l'assurance volontaire",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                tabItemName = "Description",
                textContent = "Tout salarié précédemment affilié au régime général de sécurité sociale pendant au moins six (6) mois consécutifs et qui perd son emploi, à la liberté de souscrire à une assurance volontaire pour une durée limitée à la branche des pensions. Cette souscription doit se faire dans les six (6) semaines après la rupture du contrat obligatoire. Cette opération permet d'attribuer un numéro à L'assuré volontaire en tant qu'employeur"
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Demande d'immatriculation Volontaire à l'assurance Vieillesse, invalide, décès"
                    )
                )
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                hasNotaBene = false,
                tabItemName = "Pièces à fournir",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Certificat de cessation d' activité"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Dernier bulletin de paie livret d'assurance ou carte d'assurance"
                    )
                )
            )
        )

        var contentEmbauchage = mutableListOf<Data>(
            Data(
                title = "Embauchage",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                tabItemName = "Description",
                textContent = "Il consiste à affilier le travailleur en lui attribuant un identifiant et à le rattacher à son employeur."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Avis d'embauchage."
                    )
                )
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                hasNotaBene = false,
                tabItemName = "Pièces à fournir",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "2 Photo d'identité"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Copie légalisée de l'acte de naissance"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Copie légalisée de la carte d'identité ou du passeport ou carte de résidence pour les étrangers"
                    )
                )
            )
        )

        var contentDebauchage = mutableListOf<Data>(
            Data(
                title = "Débauchage",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                tabItemName = "Description",
                textContent = "Il consiste à renseigner la date de départ du salarié d'une entreprise afin de le sortir de la liste des travailleurs de cette structure."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Imprimés",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Avis de débauchage"
                    )
                )
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                hasNotaBene = false,
                tabItemName = "Pièces à fournir",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Éventuellement lettre de licenciement, de démission ou un courrier de l'employeur"
                    )
                )
            )
        )


        //second part data

        var contentDelivranceImmatriculation = mutableListOf<Data>(
            Data(
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                tabItemName = "Description",
                textContent = "L’attestation d'immatriculation et de paiement des cotisations sociales est un document délivré à l'employeur qui en fait la demande pour toutes fins utiles. Conditions à remplir"
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Une demande écrite sur du papier en-tête de l'employeur dûment signée et cachetée"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Le reçu de paiement des cotisations sociales du dernier trimestre"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "La quittance de paiement des attestations (1000F par copie d'attestation)"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )

        var contentRetrocessionCotisations = mutableListOf<Data>(
            Data(
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Description",
                textContent = "Tout employé qui s'est vu opéré des retenues sur sa pension pour raison de non-paiement des cotisations par ses ex- employeurs lors de la liquidation de son dossier de pension peut réclamer le remboursement des retenues sur sa pension lorsque l'employeur procède au règlement partiel ou total de sa dette."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Les preuves de paiement des cotisations sociales par l'employeur"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "La photocopie de la notification de la pension"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Avis de débit"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )

        var contentLeveePrescriptionAssurance = mutableListOf<Data>(
            Data(
                title = "Levée de prescription pour\n" +
                        "assurance volontaire",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Description",
                textContent = "Tout salarié ayant perdu son emploi qui n'a pas pu déposer son dossier dans les conditions des six (6) semaines réglementaires peut saisir la commission permanente du conseil d'administration pour un recours gracieux, visant à lever cette prescription."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Photocopie carte ou livret CNSS•, * Certificat de cessation de travail"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Trois (3) dernières fiches de paie"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Demande écrite adressée à la Commission Permanente du Conseil d'Administration"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )

        var contentRemiseMajorationRetard = mutableListOf<Data>(
            Data(
                title = "Remise de majoration de retard",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Le paiement de cotisations après échéance échue",
                    "La régularisation instantanée de la situation des salariés",
                    "Le redressement suite à un contrôle. Toutefois, l'employeur peut saisir la commission permanente du conseil d' administration pour un recours gracieux en réduction desdits pénalités à la seule condition qu'ils payent la totalité des cotisations principales qui les ont générées"
                ),
                tabItemName = "Description",
                textContent = "Les majorations de retard sont des pénalités appliquées à l'employeur dans l'une des trois situations suivantes :"
            ),
            Data(
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hideTextContent = true,
                tabItemName = "Pièces complémentaires",
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Demande écrite adressée à la Commission Permanente du Conseil d'Administration"
                ),
                textContent = ""

            ),
            Data(
                isActive = false
            )
        )

        var contentDeclarationCotisations = mutableListOf<Data>(
            Data(
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Description",
                textContent = "Chaque employeur produit à la fin du mois et/ ou du trimestre en fonction du nombre de travailleurs qu'il emploie les déclarations nominatives et de cotisations sociales en précisant la période déclarée, la raison sociale, le numéro employeur et le numéro d'assurance de chacun des travailleurs y figurant."
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Formulaire de déclaration trimestrielle"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Formulaire de déclaration mensuelle"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AssuranceVieillesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RecouvrementFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}