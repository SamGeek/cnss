package co.opensi.cnss.ui.assuresbeneficiaires.presentationcnss

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import co.opensi.cnss.data.models.OrganisationItem


class OrganisationAdapter(
    private var organisationItems: MutableList<OrganisationItem>?,
    var mActivity: AppCompatActivity
) : RecyclerView.Adapter<OrganisationAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) :
        RecyclerView.ViewHolder(view) {
        var organisationNameTV: TextView
        var organisationContentTV: TextView
        var separatorView: View
        var arrowIV: ImageView

        init {
            arrowIV = view.findViewById<View>(R.id.arrow) as ImageView
            organisationNameTV = view.findViewById<View>(R.id.organisationName) as TextView
            organisationContentTV = view.findViewById<View>(R.id.organisationContent) as TextView
            separatorView = view.findViewById<View>(R.id.separator) as View
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.organisation_item, parent, false)

        itemView.setOnClickListener {
            Toast.makeText(mActivity,"toto", Toast.LENGTH_LONG).show()
        }

        return MyViewHolder(mActivity, itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val organisationItem = organisationItems!![position] as OrganisationItem
        holder.organisationNameTV.text = organisationItem.title
        holder.organisationContentTV.text = organisationItem.content


        var isDirectionGeneraleShown = true

        holder.itemView.setOnClickListener {

            if (isDirectionGeneraleShown){
                holder.organisationContentTV.visibility =  View.GONE
                holder.arrowIV.setBackgroundResource(R.drawable.ic_arrow_drop_down)//changer quand jaurai la bonne ressource
                isDirectionGeneraleShown = !isDirectionGeneraleShown
            }else{
                holder.organisationContentTV.visibility =  View.VISIBLE
                holder.arrowIV.setBackgroundResource(R.drawable.ic_up_arrow)
                isDirectionGeneraleShown = !isDirectionGeneraleShown
            }
        }

        if(position==organisationItems!!.size-1){
            holder.separatorView.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return organisationItems!!.size
    }

    fun setItems(data: MutableList<OrganisationItem>?): Unit {
        this.organisationItems = data
        notifyDataSetChanged()
    }

  
    fun addItem(item: OrganisationItem): Unit {
        this.organisationItems!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = "WeekDatesAdapter"
    }

}
