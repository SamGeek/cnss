package co.opensi.cnss.ui.assuresbeneficiaires.prestationsfamiliales

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.ComplexData
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_prestations_familiales.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [newInstance] factory method to
 * create an instance of this fragment.
 */
class PrestationsFamilialesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_prestations_familiales, container, false)

        (requireActivity()as HomeActivity).setActionBarTitle( "Prestations familiales")

        root.rl_allocations_prenatales.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_prestationsFamilialesFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentAllocationsPrenatales)
            )
        }
        root.rl_allocations_familiales.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_prestationsFamilialesFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentAllocationsFamiliales)
            )
        }
        root.rl_indemnites_journalieres.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_prestationsFamilialesFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentIndemnitesCongesMaternite)
            )
        }
        root.rl_action_sanitaire_sociale.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_prestationsFamilialesFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentActionSanitaireSociale)
            )
        }

        return root
    }

    companion object {

        var contentAllocationsPrenatales = mutableListOf<Data>(
            Data(
                title = "Allocations prénatales",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hasHeaderImage = true,
                headerImage = R.drawable.allocations_prenatales_cover,
                dotListContent = mutableListOf<String>(
                    "Le premier avant la fin du 3ème mois de grossesse",
                    "Le deuxième au cours du 6ème mois de grossesse",
                    "Le troisième au cours du 8ème mois de grossesse"
                ),
                tabItemName = "Conditions",
                textContent =  "- Etre femme salariée ou conjoint d’un travailleur salarié en état de grossesse\n" +
                        "- Subir les trois (03) examens médicaux obligatoires aux dates ci-après :"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces à fournir",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Volets des différents examens prénataux")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "1ère fraction : 1.000. Elle est due suite au premier examen prénatal",
                    "2ème fraction : 2.000. Elle est due suite au deuxième examen prénatal",
                    "3ème fraction : 1.500. Elle est due suite au troisième examen prénatal"
                ),
                hasAdditionnalTextFirst = true,
                hasAdditionnalTextSecond = false,
                additionnalTextFirst = "Tout examen non subi fait perdre au bénéficiaire la fraction correspondante",
                tabItemName = "Paiement",
                textContent = "Le montant des allocations prénatales est de 4.500 décomposé en trois (03) fractions à savoir:"
            )
        )

        var contentAllocationsFamiliales = mutableListOf<Data>(
            Data(
                title = "Allocations familiales",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hideTextContent = true,
                dotListContent = mutableListOf<String>(
                    "Être travailleur salarié ;",
                    "Être affilié à la CNSS",
                    "Justifier de six (06) mois de cotisations consécutives à la date de la première demande",
                    "Avoir des enfants à charge"
                ),
                tabItemName = "Conditions"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                hasHeaderImage = true,
                headerImage = R.drawable.prestations_familiales_cover,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Imprimés à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Demande de prestations familiales à remplir et signer"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Imprimé de certificat de vie et de charge de famille à remplir et faire signer par une autorité d’état-civil"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Imprimés de certificats de scolarité à remplir et faire signer par les responsables des établissements scolaires"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Engagement de paiement par mobile money")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à fournir par l’assuré(e)",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou du jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance du conjoint"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de mariage (facultatif)"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copies conformes ou légalisées des actes de naissance des enfants à charge"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copies des souches (volet n°3) des actes de naissances des enfants à charge"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le contrat d’apprentissage"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat d’infirmité pour les enfants malades ne pouvant ni aller à l’école ni apprendre un métier"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Deux (02) photos d’identité "),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie de la carte d’assurance de l’assuré")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = false,
                    hasRecyclerThird = false
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "La justification par le travailleur d’une activité salariée d’au moins 18 jours ou 120 heures dans le mois ",
                    "Les certificats de scolarités en début de chaque année scolaire pour les enfants âgés de 6 à 21 ans",
                    "Les visites médicales pour les enfants âgés de 0 à 6 ans",
                    "Les certificats d’apprentissage pour les enfants âgés d’au moins de 14 ans et en position d’apprentissage en début de chaque année"
                ),
                hasAdditionnalTextFirst = true,
                hasAdditionnalTextSecond = true,
                additionnalTextFirst = "Les allocations familiales sont payées trimestriellement par virement bancaire ou électronique pour chacun des enfants à charge jusqu’à la limite de 6 enfants à la fois",
                additionnalTextSecond = "NB : les allocataires sont la possibilité de cumuler jusqu’à quatre trimestres",
                tabItemName = "Paiement",
                textContent = "Le montant des allocations familiales et son mode de paiement sont fixés par décret pris en conseil des ministres. Il est actuellement de deux mille cinq cents (2.500) francs par mois et par enfant. Le paiement des allocations familiales est subordonné par :"
            )
        )

        var contentIndemnitesCongesMaternite = mutableListOf<Data>(
            Data(
                title = "Indemnités congés maternité",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                hideTextContent = false,
                textContent = "L’indemnité de congé de maternité constitue une indemnité journalière destinée à compenser la perte de salaire pendant la durée de congé de maternité. \n" +
                        "Les indemnités de congés de maternité sont dues à la femme salariée en congé de maternité.\n" +
                        "La durée du congé de maternité de quatorze (14) semaines dont six (06) avant la date présumée de l’accouchement et huit (08) après accouchement. Cette durée peut être prolongée jusqu’à quatre (04) semaines au plus suite à des complications liées à l’accouchement.\n",
                tabItemName = "Conditions"

            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = true,
                isComplexListContent = true,
                hasHeaderImage = true,
                headerImage = R.drawable.conges_maternite_cover,
                tabItemName = "Pièces à fournir",
                listContent =  mutableListOf<TextIcon>(
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Demande d’indemnité de congé de maternité à retirer à la CNSS"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Un certificat de travail qui justifie la qualité de travailleur salarié de la femme "),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Le titre de congé de maternité précisant le début du congé "),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Un certificat de cessation de travail délivré par l’employeur"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Un certificat d’examen prénatal ou certificat de grossesse délivré par un médecin ou une sage-femme"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Un certificat d’accouchement délivré par un médecin ou une sage-femme"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Un  certificat de reprise de service de la femme salariée"),
                    TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Une copie des fiches de paie du mois avant la période de congé et de la période de congé de la femme salariée ")
                )
            ),
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                hasAdditionnalTextSecond = true,
                additionnalTextSecond = "NB : Le délai d’expiration de la demande est de six (06) mois à compter de la date de reprise de l’employée",
                tabItemName = "Formalités",
                textContent = "L’indemnité journalière de congé de maternité est égale à la totalité du salaire perçu par la femme salariée au moment de la suspension de travail et est versée par l’employeur durant le congé de maternité. La CNSS rembourse à la limite de 50% cette indemnité à l’employeur. Pour ce faire, l’employeur doit constituer et déposer un dossier de demande de remboursement comprenant les pièces adéquates."
            )
        )

        var contentActionSanitaireSociale = mutableListOf<Data>(
            Data(
                title = "Action sanitaire et sociale",
                hasHeaderImage = true,
                headerImage = R.drawable.prestations_familiales_cover,
                hasOnlyTextDetails = true,
                onlyTextDetails = "L’action sanitaire et sociale est constituée essentiellement par les prestations en nature que la Caisse sert à ses assurés dans ses centres médico-sociaux. Il s’agit des consultations médicales, les soins médicaux, les expertises médicales, les vaccinations, les séances de formations et d’information sur l’hygiène nutritionnelle et familiale et tout service ayant un intérêt pour l’amélioration de la santé des bénéficiaires.  \n" +
                        "Sont bénéficiaires des actions sociales et sanitaires les femmes des travailleurs, les femmes salariées en état de grossesse ou ayant donné naissance, sous contrôle médical à un enfant et les enfants de ces femmes régulièrement inscrits au livret familial d’allocataire.\n"
            )
        )
        
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment 
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PrestationsFamilialesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}