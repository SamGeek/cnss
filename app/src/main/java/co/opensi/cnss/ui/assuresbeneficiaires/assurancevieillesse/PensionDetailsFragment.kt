package co.opensi.cnss.ui.assuresbeneficiaires.assurancevieillesse

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.assuresbeneficiaires.faq.FaqReponseFragment
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleListItemsAdapter
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleTextItemsAdapter
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.*
import kotlinx.android.synthetic.main.fragment_complex_list_content.view.*
import kotlinx.android.synthetic.main.fragment_faq_reponse.view.*
import kotlinx.android.synthetic.main.fragment_pension_details.*
import kotlinx.android.synthetic.main.fragment_pension_details.view.*
import kotlinx.android.synthetic.main.fragment_simple_list_content.view.*
import kotlinx.android.synthetic.main.fragment_simple_list_content.view.notaBene
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.*
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.recycler_text_items
import kotlinx.android.synthetic.main.fragment_simple_text_timing_content.view.*

class PensionDetailsFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var content = mutableListOf<Data>()
    private lateinit var tabLayout: TabLayout
    private lateinit var rootViewMain: View

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_pension_details, viewGroup, false)
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        content = arguments?.getParcelableArrayList<Data>("content")!!
        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter
        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))
        tabLayout = rootView.tabs
        rootViewMain = rootView//do not move this line to make the changement of the top image functionnal
        //the next line needs the rootViewMain been intialized at least once

        val data = content.get(0)

        (requireActivity()as HomeActivity).setActionBarTitle(data.title)

        if (data.hasOnlyTextDetails){
            tabLayout.visibility = View.GONE
            rootView.container.visibility = View.GONE
            rootView.onlyTextDetails.visibility = View.VISIBLE
            rootView.onlyTextDetails.text = data.onlyTextDetails
            if (data.hasHeaderImage && this::rootViewMain.isInitialized) {
                rootViewMain.headerImage.setBackgroundResource(data.headerImage)
            }
        }else {
            tabLayout.visibility = View.VISIBLE
            rootView.container.visibility = View.VISIBLE
            rootView.onlyTextDetails.visibility = View.GONE
            updateTabsState()
        }

        return rootView
    }

    private fun updateTabsState() {
        for (i in 0..mSectionsPagerAdapter!!.count - 1) {
            val data = content.get(i)

            //change top image
            if (data.hasHeaderImage && this::rootViewMain.isInitialized) {
                rootViewMain.headerImage.setBackgroundResource(data.headerImage)
            }

            if (!data.isActive) {
                tabLayout.getTabAt(i)!!.view.visibility = View.GONE
            } else {
                tabLayout.getTabAt(i)!!.setText(data.tabItemName)
            }
        }
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ConditionsAssuranceVieillesseFragment (defined as a static inner class below).
            var fragment: Fragment = retrieveAppropriateFragment(position)
            fragment.retainInstance = true

            return fragment
        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 3
        }
    }


    private fun retrieveAppropriateFragment(position: Int): Fragment {
        var data = content.get(position)
        var fragment: Fragment = SimpleTextFragment.newInstance(data)

        if (!data.isActive) {
            //si le fragement ne doit pas etre activé, on le masque juste et pi cest fini
            //trouver comment le masquer ici
            tabLayout.getTabAt(position)!!.view.visibility = View.GONE

        } else {
            tabLayout.getTabAt(position)!!.text = data.tabItemName

            //tout le traitement se fait ici
            //pour eviter de faire des choses si le fragment ne doit pas etre actif
            if (data.isTextContent) {
                //sil sagit juste d'afficher du text, on  affiche ce texte dans un fragment qui affiche du contenu texte
                //on appelle bon fragment et on lui passe le texte a afficher
                fragment = SimpleTextFragment.newInstance(data)
            } else if (data.isListContent) {
                //la on doit afficher le bon fragement avec une liste de choses
                fragment = SimpleListContentFragment.newInstance(data)
            } else if (data.isComplexListContent) {
                //la on doit afficher le bon fragement avec une liste de choses
                fragment = ComplexListContentFragment.newInstance(data)
            } else if (data.isSimpleTextTiming) {
                // la on affiche le fragment du timing avec les bonnes valeurs
                fragment = SimpleTextTimingFragment.newInstance(data)
            } else if (data.isFlexibleData){
                fragment = FlexibleContentFragment.newInstance(data)
            }
        }

        return fragment
    }

    class SimpleTextFragment : Fragment() {
        private var listItemsAdapter: SimpleTextItemsAdapter? = null
        private var textContent: String? = null
        private var data = Data()
        private var dotListData = mutableListOf<String>()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(textContentParam)!!
                textContent = data.textContent
                dotListData = data.dotListContent
            }
        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_simple_text_content,
                container,
                false
            )

            if (data.hasAdditionnalTextFirst){
                rootView.additionalTextFirst.visibility = View.VISIBLE
                rootView.additionalTextFirst.text =  data.additionnalTextFirst
            }else{
                rootView.additionalTextFirst.visibility = View.GONE
            }

            if (data.hasAdditionnalTextSecond){
                rootView.additionalTextSecond.visibility = View.VISIBLE
                rootView.additionalTextSecond.text =  data.additionnalTextSecond
            }else{
                rootView.additionalTextSecond.visibility = View.GONE
            }


            if (data.hideTextContent) {
                rootView.textContentTV.visibility = View.GONE
            } else {
                rootView.textContentTV.visibility = View.VISIBLE
                //on mets a jour avec le bon texte
                rootView.textContentTV.text = textContent
            }


            //we will add here the functionnality to show TextList
            if (data.hasTextWithDotList) {//only if it has dotList
                rootView.recycler_text_items.visibility = View.VISIBLE

                listItemsAdapter = SimpleTextItemsAdapter(
                    dotListData,
                    activity as AppCompatActivity,
                    object : SimpleTextItemsAdapter.Listener {
                        override fun onItemChoosen(textIcon: String) {
                        }
                    }
                )

                val ccLayoutManager = LinearLayoutManager(context)
                rootView.recycler_text_items.apply {
                    layoutManager = ccLayoutManager
                    itemAnimator = DefaultItemAnimator()
                    adapter = listItemsAdapter
                }
            } else {
                rootView.recycler_text_items.visibility = View.GONE
            }


            return rootView
        }

        companion object {
            private const val textContentParam = "textContentParam"
            fun newInstance(content: Data) =
                SimpleTextFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(textContentParam, content!!)
                    }
                }
        }
    }

    class SimpleListContentFragment() : Fragment() {
        private var listItemsAdapter: SimpleListItemsAdapter? = null
        private var listData = mutableListOf<TextIcon>()
        private var data = Data()


        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(SimpleListContentFragment.itemsParam)!!
                listData = data.listContent
            }
        }

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_simple_list_content,
                container,
                false
            )

            //ici on charge les données calmement
            //je vais essayer de serialiser les données en Json pour voir si ca marche sinon
            //opter pour une autre solution

            if (data.hasNotaBene){
                rootView.notaBene.visibility = View.VISIBLE
                rootView.notaBene.text = data.notaBeneContent
            }else {
                rootView.notaBene.visibility = View.GONE
            }

            listItemsAdapter = SimpleListItemsAdapter(
                listData,
                activity as AppCompatActivity,
                object : SimpleListItemsAdapter.Listener {
                    override fun onItemChoosen(textIcon: TextIcon) {
//                      Toast.makeText(requireContext(), "clicked", Toast.LENGTH_LONG).show()
                    }
                }
            )

            val ccLayoutManager = LinearLayoutManager(context)
            rootView.recycler_list_items.apply {
                layoutManager = ccLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = listItemsAdapter
            }

            return rootView
        }

        companion object {
            private const val itemsParam = "itemsParam"
            fun newInstance(content: Data) =
                SimpleListContentFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(itemsParam, content!! )
                    }
                }
        }
    }

    class ComplexListContentFragment() : Fragment() {
        private var recylclerFirstItemsAdapter: SimpleListItemsAdapter? = null
        private var recylclerSecondItemsAdapter: SimpleListItemsAdapter? = null
        private var recylclerThirdItemsAdapter: SimpleListItemsAdapter? = null
        private var data = Data()
        private lateinit var currentChoice : TextView


        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(ComplexListContentFragment.itemsParam)!!
            }
        }

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_complex_list_content,
                container,
                false
            )

            //ici on charge les données calmement
            //je vais essayer de serialiser les données en Json pour voir si ca marche sinon
            //opter pour une autre solution

            //-------------------------------------
            rootView.simpleTextFirst.text = data.complexData!!.simpleTextFirst
            rootView.boldTextFirst.text = data.complexData!!.boldTextFirst
            Functions.setBoldTextView(rootView.boldTextFirst, requireContext(), true)
            //-------------------------------------
            recylclerFirstItemsAdapter = SimpleListItemsAdapter(
                data.complexData!!.recyclerFirstContent,
                activity as AppCompatActivity,
                object : SimpleListItemsAdapter.Listener {
                    override fun onItemChoosen(textIcon: TextIcon) {
                    }
                }
            )

            val layouManagerFirst = LinearLayoutManager(context)
            rootView.recycler_view_first.apply {
                layoutManager = layouManagerFirst
                itemAnimator = DefaultItemAnimator()
                adapter = recylclerFirstItemsAdapter
            }
            //-------------------------------------
            rootView.boldTextSecond.text = data.complexData!!.boldTextSecond
            Functions.setBoldTextView(rootView.boldTextSecond, requireContext(), true)
            //-------------------------------------

            recylclerSecondItemsAdapter = SimpleListItemsAdapter(
                data.complexData!!.recyclerSecondContent,
                activity as AppCompatActivity,
                object : SimpleListItemsAdapter.Listener {
                    override fun onItemChoosen(textIcon: TextIcon) {
                    }
                }
            )

            val layouManagerSecond = LinearLayoutManager(context)
            rootView.recycler_view_second.apply {
                layoutManager = layouManagerSecond
                itemAnimator = DefaultItemAnimator()
                adapter = recylclerSecondItemsAdapter
            }

            if (data.complexData!!.hasChoiceContent){
                //and show by default the second data
                //switch the appropriate data with second data second
                //show the choice boxx
                rootView.choiceBox.visibility = View.VISIBLE
                currentChoice = rootView.choiceOne
                updateChoiceSelection(currentChoice,  rootView.choiceTwo)

                rootView.choiceOne.setOnClickListener {
                    updateChoiceSelection(rootView.choiceOne, rootView.choiceTwo)
                    recylclerSecondItemsAdapter!!.setItems(data.complexData!!.recyclerSecondContent)
                }

                rootView.choiceTwo.setOnClickListener {
                    updateChoiceSelection(rootView.choiceTwo, rootView.choiceOne)
                    recylclerSecondItemsAdapter!!.setItems(data.complexData!!.recyclerSecondBisContent)
                }

                //maintain the current selected choice box sync

            }else{
                rootView.choiceBox.visibility = View.GONE
            }

            //-------------------------------------
            //Optional data managment
            rootView.separatorSecond.visibility =
                if (data.complexData!!.hasSeparatorSecond) View.VISIBLE else View.GONE


            if (data.complexData!!.hasSimpleTextSecond) {
                rootView.simpleTextSecond.visibility = View.VISIBLE
                if (data.complexData!!.shouldSimpleTextSecondBold) {
                    rootView.simpleTextSecond.setTypeface(
                        rootView.simpleTextSecond.getTypeface(),
                        Typeface.BOLD
                    )
                }
            } else {
                rootView.separatorSecond.visibility = View.GONE
            }

            //-------------------------------------
            rootView.simpleTextSecond.text = data.complexData!!.simpleTextSecond
            //-------------------------------------

            if (data.complexData!!.hasRecyclerThird) {
                rootView.recycler_view_third.visibility = View.VISIBLE
                //we only do this, if required
                recylclerThirdItemsAdapter = SimpleListItemsAdapter(
                    data.complexData!!.recyclerThirdContent,
                    activity as AppCompatActivity,
                    object : SimpleListItemsAdapter.Listener {
                        override fun onItemChoosen(textIcon: TextIcon) {
                        }
                    }
                )

                val layouManagerThird = LinearLayoutManager(context)
                rootView.recycler_view_third.apply {
                    layoutManager = layouManagerThird
                    itemAnimator = DefaultItemAnimator()
                    adapter = recylclerThirdItemsAdapter
                }

            } else {
                rootView.recycler_view_third.visibility = View.GONE
            }


            //-------------------------------------

            return rootView
        }

        private fun updateChoiceSelection(choice: TextView?, otherChoice: TextView) {
            currentChoice = choice!!
            currentChoice.setBackgroundResource(R.drawable.border_blue_half_round)
            currentChoice.setTypeface(
                currentChoice.getTypeface(),
                Typeface.BOLD
            )
            currentChoice.setTextColor(Color.parseColor("#FFFFFF"))

            otherChoice.setBackgroundResource(0)
            otherChoice.setTextColor(Color.parseColor("#000000"))
        }

        companion object {
            private const val itemsParam = "itemsParam"
            fun newInstance(content: Data) =
                ComplexListContentFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(itemsParam, content!!)
                    }
                }
        }
    }


    class FlexibleContentFragment : Fragment() {
        private var data = Data()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(textContentParam)!!
            }
        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_flexible_content,
                container,
                false
            )

            val view = rootView.content

            data.flexibleContent.forEach {
                if (it.isTextContent) view.addView(
                    Functions.buildTextContent(
                        requireActivity(),
                        it.content,
                        it.isBold
                    )
                )
                else if (it.hasTextWithDotList) view.addView(
                    Functions.buildDotListContent(
                        requireActivity(),
                        it.dotListContent,
                        it.showDot
                    )
                )
            }



            return rootView
        }
        companion object {
            private const val textContentParam = "textContentParam"
            fun newInstance(content: Data) =
                FlexibleContentFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(textContentParam, content!!)
                    }
                }
        }
    }


    class SimpleTextTimingFragment : Fragment() {
        private var listItemsAdapter: SimpleTextItemsAdapter? = null
        private var textContent: String? = null
        private var data = Data()
        private var dotListData = mutableListOf<String>()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            arguments?.let {
                data = it.getParcelable<Data>(textContentParam)!!
                textContent = data.textContent
                dotListData = data.dotListContent
            }
        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_simple_text_timing_content,
                container,
                false
            )


            Functions.setBoldTextView(rootView.firstBoldText, requireContext(), true)
            Functions.setBoldTextView(rootView.secondBoldText, requireContext(), true)


            //nothing needed for now
            //will add later if this content change

            return rootView
        }

        companion object {
            private const val textContentParam = "textContentParam"
            fun newInstance(content: Data) =
                SimpleTextTimingFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(textContentParam, content!!)
                    }
                }
        }
    }

}