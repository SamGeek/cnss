package co.opensi.cnss.ui.assuresbeneficiaires.simulation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import co.opensi.cnss.R
import co.opensi.cnss.data.models.SimulationData
import co.opensi.cnss.utils.Functions
import kotlinx.android.synthetic.main.fragment_majoration_retard.view.*
import kotlinx.android.synthetic.main.fragment_majoration_retard.view.firstTextTV
import kotlinx.android.synthetic.main.fragment_majoration_retard.view.secondTextTV

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimulationSalaireEstimationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MajorationRetardFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var data :  SimulationData?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(
            R.layout.fragment_majoration_retard,
            container,
            false
        )

        //not needed in this case
//        root.firstTextTV.text = data!!.firstText
//        root.secondTextTV.text = data!!.secondText
//        root.thirdTextTV.text = data!!.thirdText

        root.radioYesNo!!.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radioNo) {
                //updateNow = false
                //on lui demande le montant du salaire
                root.mensualContent.visibility = View.GONE
                root.trimestrialContent.visibility = View.VISIBLE
            } else {
                //updateNow = true
                //on lui montre les périodes de simulation
                root.mensualContent.visibility = View.VISIBLE
                root.trimestrialContent.visibility = View.GONE
            }
        }

        root.validate.setOnClickListener {
            root.successContent.visibility = View.VISIBLE
            Functions.hideKeyboard(requireActivity(), requireView())
        }

        root.validateSecond.setOnClickListener {
            root.successContent.visibility = View.VISIBLE
            Functions.hideKeyboard(requireActivity(), requireView())
        }



        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Functions.hideKeyboard(requireActivity(), requireView())
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimulationSalaireEstimationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SimulationSalaireEstimationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}