package co.opensi.cnss.ui.assuresbeneficiaires.home

//import com.glide.slider.library.slidertypes.TextSliderView
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.Section
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.employeurs.recouvrement.RecouvrementFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var currentMenu: TextView
    private lateinit var secondMenu: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
//        val textView: TextView = root.findViewById(R.id.text_home)
//        homeViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })

        (requireActivity() as HomeActivity).setActionBarTitle("My CNSS")

        currentMenu = root.assureMenu
        secondMenu = root.employeurMenu
        updateCurrentMenu()


        root.employeurMenu.setOnClickListener {
            currentMenu = root.employeurMenu
            secondMenu = root.assureMenu
            updateCurrentMenu()
            root.firstLineEmployeur.visibility = View.VISIBLE
            root.firstLine.visibility = View.GONE
            showPrestationsContent.text = "Voir les services offerts"
        }

        root.assureMenu.setOnClickListener {
            currentMenu = root.assureMenu
            secondMenu = root.employeurMenu
            updateCurrentMenu()
            root.firstLineEmployeur.visibility = View.GONE
            root.firstLine.visibility = View.VISIBLE
            showPrestationsContent.text = "Voir toutes les prestations"
        }
        root.assuranceVieillesse.setOnClickListener {
            requireView().findNavController()
                .navigate(R.id.action_nav_home_to_assuranceVieillesseFragment)
        }


        root.prestationsFamiliales.setOnClickListener {
            requireView().findNavController()
                .navigate(R.id.action_nav_home_to_prestationsFamilialesFragment)
        }

        root.assuranceVolontaire.setOnClickListener {
            //directement vers pension details avec le bon contenu
            requireView().findNavController().navigate(
                R.id.action_nav_home_to_pensionDetailsFragment,
                bundleOf("content" to RecouvrementFragment.contentImmatriculationAssuranceVolontaire)
            )
        }


        root.showPrestations.setOnClickListener {
            if (currentMenu == employeurMenu) {
                //si on est sur le menu employeur, il faut lui montrer les offres
                //je dois creer un nouveau fragment pour ca , et une navigation vers...
                requireView().findNavController()
                    .navigate(R.id.action_nav_home_to_servicesOffertsFragment)
            } else {
                showPrestationsContent.text = "Voir toutes les prestations"
                requireView().findNavController()
                    .navigate(R.id.action_nav_home_to_prestationsFragment)
            }

        }

        root.risquesProfessionnels.setOnClickListener {
            requireView().findNavController()
                .navigate(R.id.action_nav_home_to_risquesProAcceuilFragment)
        }

        //le faire aller sur la faq
        root.faq.setOnClickListener {
            requireView().findNavController().navigate(R.id.action_nav_home_to_faqFragment)
        }
        root.immatriculation_affiliation.setOnClickListener {
            //on lenvoie vers immatriculation affiliation
            requireView().findNavController()
                .navigate(R.id.action_nav_home_to_immatriculationAffiliationFragment)
        }
        root.recouvrement.setOnClickListener {
            requireView().findNavController().navigate(R.id.action_nav_home_to_recouvrementFragment)
        }
        root.controle.setOnClickListener {
            //on a un nouveau contenu pour ceci
            //on redirige directement sur la vue de details
            requireView().findNavController().navigate(
                R.id.action_nav_home_to_pensionDetailsFragment,
                bundleOf("content" to contentControleEmployeur)
            )// il faut lui passer les bonnes données
        }

        root.simulation.setOnClickListener {
            requireView().findNavController().navigate(R.id.action_nav_home_to_simulationFragment)
        }

        val imageRessourceList = ArrayList<Int>()
        val listName = ArrayList<String>()
        imageRessourceList.add(
            resources.getIdentifier(
                "slide1",
                "drawable",
                requireContext().packageName
            )
        )
        listName.add("")
        imageRessourceList.add(
            resources.getIdentifier(
                "slide2",
                "drawable",
                requireContext().packageName
            )
        )
        listName.add("")
        imageRessourceList.add(
            resources.getIdentifier(
                "slide3",
                "drawable",
                requireContext().packageName
            )
        )
        listName.add("")
        imageRessourceList.add(
            resources.getIdentifier(
                "slide4",
                "drawable",
                requireContext().packageName
            )
        )
        listName.add("")

        val slider = root!!.findViewById<SliderLayout>(R.id.slider)


        // set Slider Transition Animation
        // slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetTransformer(SliderLayout.Transformer.Default)

        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider.setCustomAnimation(DescriptionAnimation())
        slider.setCustomIndicator(root.findViewById(R.id.custom_indicator))
        slider.setDuration(4000)
        slider.addOnPageChangeListener(this@HomeFragment)



        for (i in 0 until imageRessourceList.size) {
            val sliderView = TextSliderView(context)
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                .image(imageRessourceList.get(i))
//                .description(listName.get(i))
//                .setRequestOption(requestOptions)
//                .setBackgroundColor(Color.WHITE)
//                .setProgressBarVisible(true)//le quil ne faut pas commenter normalement
//                    .setOnSliderClickListener(onClick)

//        val view  = sliderView.view.findViewById<View>(R.id.description_layout)
//            view.setBackgroundColor(Color.TRANSPARENT);


            //add your extra information
            sliderView.bundle(Bundle())
            sliderView.bundle.putString("extra", listName.get(i))
            slider.addSlider(sliderView)
        }

        return root
    }

    private fun updateCurrentMenu() {


        currentMenu.background = ContextCompat.getDrawable(
            requireContext(),
            R.drawable.border_blue_half_round
        )
        currentMenu.setTextColor(Color.parseColor("#FFFFFF"))
        // currentMenu.setTypeface(Typeface.DEFAULT_BOLD)
        val sBuilder = SpannableStringBuilder()
        sBuilder.append(currentMenu.text)
        sBuilder.setSpan(
            Functions.getTypeFace(requireContext(), true),
            0,
            currentMenu.text.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        currentMenu.setText(sBuilder, TextView.BufferType.SPANNABLE)

        secondMenu.background = null
        secondMenu.setTextColor(Color.parseColor("#000000"))
        // secondMenu.setTypeface(Typeface.DEFAULT)
        val sBuilderSecond = SpannableStringBuilder()
        sBuilderSecond.append(secondMenu.text)
        sBuilderSecond.setSpan(
            Functions.getTypeFace(requireContext(), false),
            0,
            secondMenu.text.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        secondMenu.setText(sBuilderSecond, TextView.BufferType.SPANNABLE)

        if (currentMenu == employeurMenu) {
            showPrestationsContent.text = "Voir les services offerts"
        }
    }


    companion object {
        var contentControleEmployeur = mutableListOf<Data>(
            Data(
                title = "Contrôle employeurs",
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                isActive = true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(//0
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le contrôle employeurs est l’ensemble des mécanismes mis en place pour la vérification de l’assiette des cotisations, de l’immatriculation des employeurs, de l’affiliation de tous les travailleurs et du paiement des cotisations sociales dans les délais prescrits. "
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le système de détermination des cotisations étant déclaratif, c’est l’employeur qui affilie ses travailleurs et détermine lui-même le montant des cotisations dues à la Caisse Nationale de Sécurité Sociale (CNSS). La contrepartie de ce système déclaratif est l’obligation pour la CNSS de procéder au contrôle de l’exactitude des déclarations fournies par le cotisant. "
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’objectif essentiel du contrôle est d’assurer non seulement la sincérité et l’exactitude des déclarations de cotisations mais aussi, et surtout, l’équité pour l’ensemble des cotisants."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le contrôle de l’assiette des cotisations s’effectue en rapprochant le montant des rémunérations versées aux travailleurs et relevé dans les documents comptables de l’employeur du montant des salaires déclarés à la Caisse Nationale de Sécurité Sociale par employeur."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La détermination du montant des rémunérations versées aux travailleurs nécessite l’exploitation du registre des salaires, des états financiers et autres documents comptables et administratifs."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En cas de non respect des prescriptions légales en la matière, les contrôleurs et inspecteurs de la Caisse font recours à l’article 27 du code de sécurité sociale relatif aux majorations de retard et au recouvrement forcé des cotisations pour amener l’employeur à s’acquitter de ses obligations."
                    )
                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Contrôle"
            ),
            Data(
                isActive = true,
                isFlexibleData = true,
                flexibleContent =
                mutableListOf<Section>(//0
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les employeurs sont tenus de recevoir et de communiquer aux contrôleurs et inspecteurs de la Caisse Nationale de Sécurité Sociale les documents requis pour leur action de contrôle. Les obstacles au contrôle dûment constatés sont passibles de sanctions."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’opposition au contrôle est caractérisée lorsque l’employeur :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "fait preuve d’atermoiements inexcusables ou de réticence ;",
                            "refuse de présenter les documents requis ou s’oppose à l’entrée dans l’entreprise ;",
                            "s’absente sans laisser d’instruction à ses collaborateurs, alors qu’il était prévenu de la visite ;",
                            "fournit des renseignements volontairement inexacts."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Ces obstacles au contrôle sont passibles de peines allant du paiement d’amendes de 25.000 à 250.000 francs CFA à des peines de 1 à 3 mois d’emprisonnement."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Des sanctions allant de simples amendes à des peines d’emprisonnement sont également encourues par les employeurs pour :"
                    ),
                    // je mets un dot list ici
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "défaut d’immatriculation ;",
                            "défaut de paiement des cotisations ;",
                            "rétention indue de précompte ;",
                            "outrage et violences contre les agents de contrôle ;",
                            "défaut de déclaration d’accident du travail ;",
                            "défaut de production de la déclaration nominative des salaires."
                        )
                    )

                ),
                isTextContent = false,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Sanctions"
            ),
            Data(
                isActive = false
            )
        )
    }

    override fun onSliderClick(slider: BaseSliderView?) {
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }
}