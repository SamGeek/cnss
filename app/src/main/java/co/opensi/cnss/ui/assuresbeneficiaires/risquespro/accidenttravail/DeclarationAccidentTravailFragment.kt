package co.opensi.cnss.ui.assuresbeneficiaires.risquespro.accidenttravail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import co.opensi.cnss.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_pension_details.view.*

class DeclarationAccidentTravailFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val rootView  = inflater.inflate(R.layout.fragment_declaration_accident_travail, viewGroup, false)
//        requireActivity().setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)

        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter

        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))

        return rootView
    }



    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ImprimeFragment (defined as a static inner class below).
            lateinit var fragment : Fragment

            if (position == 0){
                fragment  =
                    ImprimeFragment.newInstance()

            }else if( position == 2){
                fragment =
                    ProcessusTraitementFragment.newInstance()
            }else{
                fragment =
                    PiecesComplementairesFragment.newInstance()
            }

            fragment.retainInstance = true
            return  fragment

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 3
        }
    }



    /**
     * A placeholder fragment containing a custom view now.
     */
    class ImprimeFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_imprime,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): ImprimeFragment {
                val fragment =
                    ImprimeFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }


    class PiecesComplementairesFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_pieces_complementaires,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): PiecesComplementairesFragment {
                val fragment =
                    PiecesComplementairesFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

    class ProcessusTraitementFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_processus_traitement,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): ProcessusTraitementFragment {
                val fragment =
                    ProcessusTraitementFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }



}