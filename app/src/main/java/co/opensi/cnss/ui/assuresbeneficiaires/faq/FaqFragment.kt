package co.opensi.cnss.ui.assuresbeneficiaires.faq

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.FaqItem
import co.opensi.cnss.data.models.Prestation
import co.opensi.cnss.ui.assuresbeneficiaires.assurancevieillesse.AssuranceVieillesseFragment
import co.opensi.cnss.ui.assuresbeneficiaires.prestations.PrestationsAdapter
import co.opensi.cnss.ui.assuresbeneficiaires.prestations.PrestationsFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions.Companion.afterTextChanged
import kotlinx.android.synthetic.main.fragment_faq.view.*
import kotlinx.android.synthetic.main.fragment_faq_reponse.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AssuranceVieillesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FaqFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var prestationAdapter: PrestationsAdapter? = null
    private var faqItemsAdapter: FaqItemAdapter? = null
    private var faqItemsFiltered = mutableListOf<FaqItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_faq, container, false)

        (requireActivity()as HomeActivity).setActionBarTitle("FAQ")


        //bon on a 2 recyclers a populer et donc 2 actions de suivi a faire
        //il faut un adapter pour le texte simple

        faqItemsAdapter = FaqItemAdapter(
            faqItems,//lui donner les bon elements
            activity as AppCompatActivity,
            object : FaqItemAdapter.Listener {
                override fun onFaqItemChoosen(faqItem: FaqItem) {
                    //ici on ne fait rien pour linstant
                    //quand toutes les interfaces seront faites, on verra ou emmener ceci avec le bon id etc...
                    requireView().findNavController().navigate(R.id.action_faqFragment_to_faqReponseFragment,
                        bundleOf("content" to faqItem))

                }
            }
        )

        val faqLayoutManager = LinearLayoutManager(context)
        rootView.recycler_faq_questions.apply {
            layoutManager = faqLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = faqItemsAdapter
        }


        //on va implémenter la recherche ici
        rootView.searchText.afterTextChanged {
            //Toast.makeText(requireContext(),it+" length "+it.length ,Toast.LENGTH_LONG).show()

            if(it.length==0){
                //on affiche  la liste initiale
                //ceci est utile quand on supprime progressivement tout le contenu du champ de recherche juska obtenir une liste vide
                faqItemsAdapter!!.setItems(faqItems)
            }else{
                //on filtre la liste avec le pattern envoyé
                val newResults = mutableListOf<FaqItem>()
                val result  = FaqQuestionsFragment.faqItems.toMutableList().forEach {
                    faqItems -> newResults.addAll(faqItems.filter { faqItem -> faqItem.content.contains(it, true)  })
                }
                faqItemsFiltered  = newResults.toMutableList()
                faqItemsAdapter!!.setItems(faqItemsFiltered)
                //gérer l'empty state quand aucun service ne corresponds a la recherche
            }

        }


        //pour le 2e cest a peu pres ceci mais faut customiser les actions plutard
        prestationAdapter = PrestationsAdapter(
            prestations,//lui donner les bon elements
            activity as AppCompatActivity,
            object : PrestationsAdapter.Listener {
                override fun onPrestationChoosen(prestation: Prestation) {
                    //on part sur le faq questions faq questions avec le bon id
                    requireView().findNavController().navigate(R.id.action_faqFragment_to_faqQuestionsFragment,
                        bundleOf("content" to prestation))
                    //normalement en fonction de la prestation choisie, on doit donner les bonnes data a afficher dans la section suivante, mais pour linstant
                    // dans un but de test, on va simplement rediriger et faire des tests
                }
            }
        )

        val ccLayoutManager = GridLayoutManager(context, 3)
        rootView.recycler_choose_faq_prestation.apply {
            layoutManager = ccLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = prestationAdapter
        }

        return rootView
    }

    companion object {

        val faqItems = mutableListOf<FaqItem>(
            FaqItem(7,"Quels sont les montants des allocations\n" +
                    "familiales ?"),
            FaqItem(24,"Quelles sont les formalités à remplir en cas d’accident du travail ou de maladie professionnelle ?\n"),
            FaqItem(13,"Qu'est-ce qu'une allocation de vieillesse ?"),
            FaqItem(43,"Quelles sont les conditions à remplir pour bénéficier des droits des travailleurs migrants ?")
        )

        //il faut aussi gerer la destination lorsqu'on clique sur certaines des catégories
        //update les prestations later avec libeles et icones
        val prestations = mutableListOf<Prestation>(
            Prestation(0,R.drawable.ic_allocations_familiales,"Prestations familiales"),
            Prestation(1,R.drawable.ic_pension_vieillesse_normale,"Pension de vieillesse"),
            Prestation(2,R.drawable.ic_maladies_pro,"Risques professionnels"),
            Prestation(3,R.drawable.ic_pension_vieillesse_normale,"Recouvrement des cotisations"),
            Prestation(4,R.drawable.ic_pension_survivant_veuf,"Droit des travailleurs migrants")
        )

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AssuranceVieillesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FaqFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}