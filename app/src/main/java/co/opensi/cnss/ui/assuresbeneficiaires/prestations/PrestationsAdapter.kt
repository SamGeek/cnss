package co.opensi.cnss.ui.assuresbeneficiaires.prestations

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import co.opensi.cnss.data.models.Prestation


class PrestationsAdapter(
    private var prestations: MutableList<Prestation>?,
    var mActivity: AppCompatActivity,
    var listener: Listener
) : RecyclerView.Adapter<PrestationsAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val context: Context, view: View) :
        RecyclerView.ViewHolder(view) {
        var prestationImage: ImageView
        var prestationLabel: TextView

        init {
            prestationImage = view.findViewById<View>(R.id.iv_image) as ImageView
            prestationLabel = view.findViewById<View>(R.id.prestation_label) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.prestation_item, parent, false)

        itemView.setOnClickListener {
            Toast.makeText(mActivity,"toto", Toast.LENGTH_LONG).show()
        }

        return MyViewHolder(mActivity, itemView)
    }

    interface Listener {
        fun onPrestationChoosen(prestation: Prestation)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val prestation = prestations!![position] as Prestation
        holder.prestationImage.setBackgroundResource(prestation.photo)
        holder.prestationLabel.text = prestation.libelle

        holder.itemView.setOnClickListener {
            //Toast.makeText(mActivity,"Click", Toast.LENGTH_LONG).show()
            listener.onPrestationChoosen(prestations!!.get(position))
        }
    }

    override fun getItemCount(): Int {
        return prestations!!.size
    }

    fun setItems(data: MutableList<Prestation>?): Unit {
        this.prestations = data
        notifyDataSetChanged()
    }

  
    fun addItem(item: Prestation): Unit {
        this.prestations!!.add(item!!)
        notifyDataSetChanged()
    }

    companion object {
        private val TAG = "WeekDatesAdapter"
    }

}
