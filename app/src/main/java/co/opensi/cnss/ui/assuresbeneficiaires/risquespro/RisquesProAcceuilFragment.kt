package co.opensi.cnss.ui.assuresbeneficiaires.risquespro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_risques_professionnels.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AssuranceVieillesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RisquesProAcceuilFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_risques_professionnels, container, false)

        (requireActivity()as HomeActivity).setActionBarTitle( "Risques professionnels")

        root.rl_accident_travail.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_risquesProAcceuilFragment_to_accidentTravailFragment,
                bundleOf("choice" to true))
        }

        root.rl_maladies_professionnelles.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_risquesProAcceuilFragment_to_accidentTravailFragment,
                bundleOf("choice" to false))
        }



        return root



    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AssuranceVieillesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RisquesProAcceuilFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}