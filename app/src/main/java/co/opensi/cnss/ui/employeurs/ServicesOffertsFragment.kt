package co.opensi.cnss.ui.employeurs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.ComplexData
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.employeurs.recouvrement.RecouvrementFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.*
import kotlinx.android.synthetic.main.fragment_recouvrement.view.*
import kotlinx.android.synthetic.main.fragment_service_offert.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AssuranceVieillesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ServicesOffertsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_service_offert, container, false)
        (requireActivity()as HomeActivity).setActionBarTitle("Services offerts")

        root.rl_immatriculation_versement_cotisation.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_servicesOffertsFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentImmatriculationVersementCotisation)
            )
        }

        root.rl_levee_prescription_assurance_volontaire.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_servicesOffertsFragment_to_pensionDetailsFragment,
                bundleOf("content" to RecouvrementFragment.contentLeveePrescriptionAssurance)
            )
        }

        root.rl_remise_majoration_retard.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_servicesOffertsFragment_to_pensionDetailsFragment,
                bundleOf("content" to RecouvrementFragment.contentRemiseMajorationRetard)
                )
        }


        return root
    }

    companion object {


        var contentImmatriculationVersementCotisation = mutableListOf<Data>(
            Data(
                title = "Délivrance d'attestation d'immatriculation et de versement des cotisations",
                isActive = true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                dotListContent = mutableListOf<String>(
                    "Être Immatriculé à la CNSS",
                    "Être à jour de ses cotisations."
                ),
                tabItemName = "Description",
                textContent = "L’attestation d'immatriculation et de paiement des cotisations sociales est un document délivré à l'employeur qui en fait la demande pour toutes fins utiles. Conditions à remplir"
            ),
            Data(
                isActive = true,
                isTextContent = false,
                isListContent = true,
                tabItemName = "Pièces complémentaires",
                listContent = mutableListOf<TextIcon>(
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Une demande écrite sur du papier en-tête de l'employeur dûment signée et cachetée"                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "Le reçu de paiement des cotisations sociales du dernier trimestre"
                    ),
                    TextIcon(
                        icon = R.drawable.ic_piece_justificative_orange,
                        texte = "La quittance de paiement des attestations (1000F par copie d'attestation)"
                    )
                )
            ),
            Data(
                isActive = false
            )
        )


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AssuranceVieillesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ServicesOffertsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}