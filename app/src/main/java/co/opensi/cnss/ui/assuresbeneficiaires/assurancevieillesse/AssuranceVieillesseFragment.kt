package co.opensi.cnss.ui.assuresbeneficiaires.assurancevieillesse

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import co.opensi.cnss.R
import co.opensi.cnss.data.models.ComplexData
import co.opensi.cnss.data.models.Data
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.employeurs.recouvrement.RecouvrementFragment
import co.opensi.cnss.ui.welcome.HomeActivity
import kotlinx.android.synthetic.main.fragment_assurance_vieillesse.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AssuranceVieillesseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AssuranceVieillesseFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_assurance_vieillesse, container, false)




        (requireActivity()as HomeActivity).setActionBarTitle("Assurance Vieillesse")

        root.rl_pension_vieillesse_normale.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentPensionVieillesseNoramle)
            )
        }

        root.rl_pension_vieillesse_anticipee.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentPensionVieillesseAnticipée)
            )
        }

        root.rl_pension_invalidité.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to AssuranceVieillesseFragment.contentPensionInvalidite)
                )
        }
        //---

        root.rl_allocation_vieillesse.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentAllocationVieillesse)
            )
        }
        root.rl_pension_survivant_veuf.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentPensionVeuf)
            )
        }
        root.rl_pension_survivant_orphelin.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentPensionSurvivantOrphelin)
            )
        }

        //check this
        root.rl_allocation_survivant_veuf.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentSurvivantVeuf)
            )
        }


        root.rl_allocation_survivant_orphelin.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentAllocationSurvivantOrphelin)
            )
        }
        root.rl_allocation_survivant_ascendant.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_assuranceVieillesseFragment_to_pensionDetailsFragment,
                bundleOf("content" to contentAllocationSurvivantAscendant)
            )
        }

        return root
    }

    companion object {


        var contentPensionVieillesseNoramle = mutableListOf<Data>(
            Data(
                title = "Pension de vieillesse normale",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                dotListContent = mutableListOf<String>(
                    "Avoir cessé toute activité salariale ;",
                    "Avoir totalisé au moins cent quatre-vingt (180) mois effectifs de cotisations sociales."
                ),
                tabItemName = "Conditions",
                textContent =  "L’assuré qui a atteint l’âge de 60 ans a droit à une pension de vieillesse normale s’il remplit les conditions suivantes :"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de vieillesse à remplir et signer"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Une attestation d’immatriculation en cas de perte de son livret ou carte d’assurance")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par l’assuré(e)",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "La copie conforme et légalisée de l’acte de naissance ou jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toute pièce justificative de carrière professionnelle"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des ayants-droits (facultatifs)"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de mariage (facultatif)")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = false,
                    simpleTextSecond = "Si l’assuré est allocataire et ayant des enfants inscrits au livret familial allocataire de moins de 21 ans, produire en plus :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité des enfants ou certificat d’apprentissage ou certificat d’infirmité"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "Avoir cessé toute activité salariale ;",
                    "Avoir totalisé au moins cent quatre-vingt (180) mois effectifs de cotisations sociales."
                )
            )
        )

        var contentPensionVieillesseAnticipée = mutableListOf<Data>(
            Data(
                title = "Pension de vieillesse anticipée",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                dotListContent = mutableListOf<String>(
                    "Avoir cessé toute activité salariale ;",
                    "Avoir totalisé au moins cent quatre-vingt (180) mois effectifs de cotisations sociales."
                ),
                tabItemName = "Conditions",
                textContent =  "Tout assuré qui a atteint au moins l’âge de 55 ans peut demander la jouissance d’une pension de vieillesse anticipée s’il remplit les conditions suivantes :"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de vieillesse anticipée à remplir et signer"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Une attestation d’immatriculation en cas de perte de son livret ou carte d’assurance")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par l’assuré(e)",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme et légalisée de l’acte de naissance ou jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toute pièce justificative de carrière professionnelle"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des ayants-droits (facultatifs)"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de mariage (facultatif)")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = false,
                    simpleTextSecond = "Si l’assuré est allocataire et ayant des enfants inscrits au livret familial allocataire de moins de 21 ans, produire en plus :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité des enfants ou certificat d’apprentissage ou certificat d’infirmité"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )

        var contentPensionInvalidite = mutableListOf<Data>(
            Data(
                title = "Pension d'invalidité",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                dotListContent = mutableListOf<String>(
                    "Etre un travailleur en activité ;",
                    "Perdre au moins 2/3 de ses capacités physiques et intellectuelles des suites d’un accident ou d’une maladie non professionnelle ;",
                    "Avoir totalisé au moins soixante (60) mois effectifs de cotisations sociales ;",
                    "Avoir cotisé obligatoirement six (06) au cours des douze (12) derniers mois précédant le début de l’incapacité conduisant à l’invalidité."
                ),
                tabItemName = "Conditions",
                textContent =  "L’assuré en activité qui devient invalide avant d’atteindre l’âge de 60 ans a droit à une pension d’invalidité s’il remplit les conditions suivantes :"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de vieillesse anticipée à remplir et signer"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Une attestation d’immatriculation en cas de perte de son livret ou carte d’assurance")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par l’assuré(e)",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme et légalisée de l’acte de naissance ou jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toute pièce justificative de carrière professionnelle"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des ayants-droits (facultatifs)"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de mariage (facultatif)")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = false,
                    simpleTextSecond = "Si l’assuré est allocataire et ayant des enfants inscrits au livret familial allocataire de moins de 21 ans, produire en plus :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité des enfants ou certificat d’apprentissage ou certificat d’infirmité"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )


        var contentAllocationVieillesse = mutableListOf<Data>(
            Data(
                title = "Allocation de vieillesse",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = true,
                hasHeaderImage = true,
                headerImage = R.drawable.controle_employeur,
                dotListContent = mutableListOf<String>(
                    "Avoir totalisé au moins douze (12) mois effectifs de cotisations sociales."
                ),
                tabItemName = "Conditions",
                textContent =  "Tout assuré qui a atteint au moins l’âge de 55 ans, a cessé toute activité salariale alors qu’il ne satisfait pas la condition de 180 mois d’assurance, a droit à une allocation de vieillesse s’il remplit la condition suivante : "
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de vieillesse anticipée à remplir et signer"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Une attestation d’immatriculation en cas de perte de son livret ou carte d’assurance")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par l’assuré(e)",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Le livret ou la carte d’assurance"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Copie conforme et légalisée de l’acte de naissance ou jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_photo_identite_orange, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite_orange, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "Toute pièce justificative de carrière professionnelle")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = false,
                    hasRecyclerThird = false
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )

        var contentPensionVeuf = mutableListOf<Data>(
            Data(
                title = "Pension de survivant veuve/veuf",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                hasHeaderImage = true,
                headerImage = R.drawable.prestations_familiales_cover,
                tabItemName = "Conditions",
                textContent =  "Etre conjoint d’un pensionné décédé ou d’un assuré qui à la date de son décès remplissait les conditions requises pour bénéficier d’une pension"
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de survivant à remplir et signer")
                    ),
                    hasChoiceContent = true,
                    boldTextSecond = "Pièces à compléter par le bénéficiaire",
                    choiceOptionFirst = "Cas d’un assuré \ndécédé en activité",
                    choiceOptionSecond = "Cas d’un pensionné \ndécédé",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de mariage ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Certificat de non divorce, de non remariage, de non séparation de corps et non concubinage notoire ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    recyclerSecondBisContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme et légalisée de l’acte de naissance ou jugement supplétif"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toute pièce justificative de carrière professionnelle")
                    ),
                    hasSeparatorSecond =  false,
                    hasSimpleTextSecond = false,
                    hasRecyclerThird = false
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )


        var contentPensionSurvivantOrphelin = mutableListOf<Data>(
            Data(
                title = "Pension de survivant orphelin",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Conditions",
                textContent =  "Avoir à charge les enfants de moins de 21 ans d’un pensionné décédé ou d’un assuré qui, à la date de son décès, remplissait les conditions requises pour bénéficier d’une pension."
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                hasHeaderImage = true,
                headerImage = R.drawable.vieillesse_header,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier de pension anticipée comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de survivant à remplir et signer")
                    ),
                    hasChoiceContent = true,
                    boldTextSecond = "Pièces à compléter par le bénéficiaire",
                    choiceOptionFirst = "Cas d’un assuré \ndécédé en activité",
                    choiceOptionSecond = "Cas d’un pensionné \ndécédé",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Une ordonnance du tribunal désignant le demandeur tuteur des enfants ou une attestation de garde d’enfants délivrée par un centre de promotion social ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des enfants ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Un certificat de vie et charge de famille ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité ou d’apprentissage ou d’infirmité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    recyclerSecondBisContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès du pensionné ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Une ordonnance du tribunal désignant le demandeur tuteur des enfants ou une attestation de garde d’enfants délivrée par un centre de promotion social ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Les actes de naissance des enfants ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Un certificat de vie et charge de famille ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Photocopie d’une pièce d’identité ;")
                    ),
                    hasSeparatorSecond =  true,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = true,
                    simpleTextSecond = "NB : lorsque le demandeur de l’allocation de survivant orphelin est la veuve ou le veuf, en plus des pièces de l’allocation de survivant veuve, le demandeur complète pour le compte de l’allocation de survivant orphelin seulement :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "La demande de pension survivant"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des enfants ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité ou d’apprentissage ou d’infirmité"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Une photo d’identité")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )


        var contentSurvivantVeuf = mutableListOf<Data>(
            Data(
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                hasHeaderImage = true,
                headerImage = R.drawable.vieillesse_header,
                tabItemName = "Conditions",
                textContent =  "Etre conjoint d’un assuré décédé comptant, à la date de son décès, moins de 180 mois d’assurance et au moins douze (12) mois d’assurance."
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de survivant à remplir et signer")
                    ),
                    hasChoiceContent = true,
                    boldTextSecond = "Pièces à compléter par le bénéficiaire",
                    choiceOptionFirst = "Cas d’un assuré \ndécédé en activité",
                    choiceOptionSecond = "Cas d’un pensionné \ndécédé",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de mariage ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Certificat de non divorce, de non remariage, de non séparation de corps et non concubinage notoire ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    recyclerSecondBisContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Copie conforme ou légalisée de l’acte de mariage ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Certificat de non divorce, de non remariage, de non séparation de corps et non concubinage notoire ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Photocopie d’une pièce d’identité ;"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    hasSeparatorSecond =  true,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = true,
                    simpleTextSecond = "NB : lorsque le demandeur de l’allocation de survivant orphelin est la veuve ou le veuf, en plus des pièces de l’allocation de survivant veuve, le demandeur complète pour le compte de l’allocation de survivant orphelin seulement :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "La demande de pension survivant"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des enfants"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité ou d’apprentissage ou d’infirmité"),
                        TextIcon(icon = R.drawable.ic_photocopie_identite, texte = "Une photo d’identité")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )


        //il y a un menu incompris j'ai demandé a Vital pour plus de clarifications

        var contentAllocationSurvivantOrphelin = mutableListOf<Data>(
            Data(
                title = "Allocation de survivant orphelins",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                hasHeaderImage = true,
                headerImage = R.drawable.vieillesse_header,
                tabItemName = "Conditions",
                textContent =  "Etre conjoint d’un assuré décédé comptant, à la date de son décès, moins de 180 mois d’assurance et au moins douze (12) mois d’assurance."
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de survivant à remplir et signer")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par le bénéficiaire",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de mariage ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Certificat de non divorce, de non remariage, de non séparation de corps et non concubinage notoire ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    hasSeparatorSecond =  true,
                    hasSimpleTextSecond = true,
                    shouldSimpleTextSecondBold = true,
                    simpleTextSecond = "NB : lorsque le demandeur de l’allocation de survivant orphelin est la veuve ou le veuf, en plus des pièces de l’allocation de survivant veuve, le demandeur complète pour le compte de l’allocation de survivant orphelin seulement :",
                    hasRecyclerThird = true,
                    recyclerThirdContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "La demande de pension survivant"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les actes de naissance des enfants"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le certificat de vie et de charge de famille"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Les certificats de scolarité ou d’apprentissage ou d’infirmité"),
                        TextIcon(icon = R.drawable.ic_photo_identite, texte = "Une photo d’identité")
                    )
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )

        var contentAllocationSurvivantAscendant = mutableListOf<Data>(
            Data(
                title = "Allocation de survivant ascendant",
                isActive=true,
                isTextContent = true,
                isListContent = false,
                hasTextWithDotList = false,
                tabItemName = "Conditions",
                hasHeaderImage = true,
                headerImage = R.drawable.vieillesse_header,
                textContent =  "Etre un ascendant direct de l’assuré décédé qui à la date de son décès n’a ni époux, ni enfant."
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isListContent = false,
                isComplexListContent = true,
                tabItemName = "Pièces à fournir",
                complexData = ComplexData(
                    simpleTextFirst = "Constituer un dossier comprenant les pièces suivantes :",
                    boldTextFirst= "Pièces à retirer aux guichets de la CNSS",
                    recyclerFirstContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_orange, texte = "La demande de pension de survivant à remplir et signer")
                    ),
                    hasChoiceContent = false,
                    boldTextSecond = "Pièces à compléter par le bénéficiaire",
                    recyclerSecondContent = mutableListOf<TextIcon>(
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "L’acte de décès de l’assuré"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Copie conforme ou légalisée de l’acte de naissance ou jugement supplétif du demandeur ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Trois (03) photos d’identité ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Le livret ou la carte d’assurance ou une attestation d’immatriculation de l’assuré décédé ;"),
                        TextIcon(icon = R.drawable.ic_piece_justificative_bleue, texte = "Toutes pièces justificatives de la carrière de l’assuré décédé.")
                    ),
                    hasSeparatorSecond =  true,
                    hasSimpleTextSecond = false,
                    shouldSimpleTextSecondBold = false,
                    hasRecyclerThird = false
                )
            ),
            Data(
                isActive=true,
                isTextContent = false,
                isSimpleTextTiming = true,
                tabItemName = "Délai de prescription",
                //im using the dotlist content not to create more usless variables
                //but can change this later if needed
                dotListContent = mutableListOf<String>(
                    "",
                    ""
                )
            )
        )


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AssuranceVieillesseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AssuranceVieillesseFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}