package co.opensi.cnss.ui.assuresbeneficiaires.presentationcnss

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import co.opensi.cnss.R
import co.opensi.cnss.data.models.OrganisationItem
import co.opensi.cnss.data.models.TextIcon
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleListItemsAdapter
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleTextItemsAdapter
import co.opensi.cnss.ui.welcome.HomeActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_organisation_cnss.view.*
import kotlinx.android.synthetic.main.fragment_pension_details.view.*
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.*

class PresentationCnssFragment : Fragment() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        (requireActivity() as HomeActivity).setActionBarTitle("Présentation de la CNSS")


        val rootView  = inflater.inflate(R.layout.fragment_presentation_cnss, viewGroup, false)
//        requireActivity().setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)


        // Set up the ViewPager with the sections adapter.
        rootView.container.adapter = mSectionsPagerAdapter

        rootView.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(rootView.tabs))
        rootView.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(rootView.container))



        return rootView
    }



    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a MissionCnssFragment (defined as a static inner class below).
            lateinit var fragment : Fragment

            if (position == 0){
                fragment  =
                    MissionCnssFragment.newInstance()

            }else if( position == 2){
                fragment =
                    OrganigrammeCnssFragment.newInstance()
            }else{
                fragment =
                    OrganisationCnssFragment.newInstance()
            }

            fragment.retainInstance = true
            return  fragment

        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 3
        }
    }



    /**
     * A placeholder fragment containing a custom view now.
     */
    class MissionCnssFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_mission_cnss,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): MissionCnssFragment {
                val fragment =
                    MissionCnssFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }



    class OrganisationCnssFragment : Fragment() {
        private var organisationItemsAdapter: OrganisationAdapter? = null


        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_organisation_cnss,
                container,
                false
            )

            organisationItemsAdapter = OrganisationAdapter(
                organisationItems,
                activity as AppCompatActivity
            )

            val ccLayoutManager = LinearLayoutManager(context)
            rootView.recycler_organisation_items.apply {
                layoutManager = ccLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = organisationItemsAdapter
            }


            return rootView
        }

        companion object {
            var organisationItems = mutableListOf<OrganisationItem>(
                OrganisationItem(
                   title = "Direction générale",
                   content = "Elle applique, sous l’autorité du Conseil d’Administration, la politique générale de la Caisse. A ce titre, elle coordonne les activités qui sont menées au sein des différentes structures et veille à leur bon fonctionnement."
                ),
                OrganisationItem(
                    title = "Direction de l’Audit Interne et de l’Inspection",
                    content = "Elle est chargée du contrôle a priori et a posteriori de toutes les structures de la CNSS, de l’analyse des réclamations des employeurs et des assurés sociaux. Elle procède aux vérifications et aux enquêtes en cas de malversations ou faits assimilés commis par des tiers ou par des agents de la Caisse."
                ),
                OrganisationItem(
                    title = "Direction des Ressources Humaines",
                    content = "Elle est chargée de la gestion des ressources humaines, de la solde du personnel et des archives."
                ),
                OrganisationItem(
                    title = "Direction du Recouvrement",
                    content = "Elle s’occupe du recouvrement des cotisations, de l’immatriculation des employeurs, de l’affiliation des travailleurs et de la gestion des comptes cotisants."
                ),
                OrganisationItem(
                    title = "Direction des Prestations",
                    content = "Elle s’occupe de la gestion de toutes les branches de prestations, de la gestion de la carrière des travailleurs affiliés et de la prévention des risques professionnels."
                ),
                OrganisationItem(
                    title = "Direction du Budget et du Patrimoine",
                    content = "Elle est chargée de l’élaboration et de l’exécution du budget autonome de la CNSS, de l’approvisionnement en matériel et de la gestion immobilière.\n"
                ),
                OrganisationItem(
                    title = "Direction Financière et Comptable",
                    content = "Elle a pour attribution la tenue de la comptabilité, la gestion de la trésorerie et des placements et le paiement des dépenses techniques et administratives. Conformément au code des marchés publics, il a été créé la structure dénommée « Personne responsable des marchés publics » en charge de conduire la procédure  de commande des biens et services de la CNSS. "
                )


            )
            fun newInstance(): OrganisationCnssFragment {
                val fragment =
                    OrganisationCnssFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }

    class OrganigrammeCnssFragment : Fragment() {

        //private var mAdapter: ProviderAdapter? = null
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val rootView = inflater.inflate(
                R.layout.fragment_organigramme_cnss,
                container,
                false
            )

            return rootView
        }

        companion object {
            fun newInstance(): OrganigrammeCnssFragment {
                val fragment =
                    OrganigrammeCnssFragment()
                val args = Bundle()
                //args.putBoolean(ARG_IS_POSTPAID, isPostPaid)
                fragment.arguments = args
                return fragment
            }
        }
    }



}