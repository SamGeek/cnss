package co.opensi.cnss.ui.welcome

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import co.opensi.cnss.R
import co.opensi.cnss.app.App
import com.rd.PageIndicatorView
import com.rd.animation.type.AnimationType
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.android.synthetic.main.activity_app_intro.*
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.*
import java.util.*


class AppIntroActivity : AppCompatActivity() {

    private var pageIndicatorView: PageIndicatorView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_intro)
        initViews()
        toggleHideBar(this)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    private fun initViews() {
        val adapter = HomeAdapter()
        adapter.setData(createPageList())
        val pager: ViewPager = findViewById(R.id.appintro_viewpager)
        pager.adapter = adapter
        pageIndicatorView = findViewById(R.id.pageIndicatorView)
        pageIndicatorView!!.setAnimationType(AnimationType.WORM)
        pageIndicatorView!!.setSelectedColor(R.color.primary_blue)
        pageIndicatorView!!.setUnselectedColor(Color.parseColor("#DADEE3"))
        pageIndicatorView!!.setRadius(6)
        begin.setOnClickListener {
            //sur le click de ce boutton on doit enregistrer la preference et aller sur le home
            //et la prochaine fois on ne doit plus atterir sur ce ecran
            App.appPreferences.isOnboardingDone = true
            startActivity(Intent(this@AppIntroActivity, HomeActivity::class.java))
            this@AppIntroActivity.finish()
        }
    }


    private fun createPageList(): List<View> {

        val view1 = View.inflate(this, R.layout.onboarding_third_screen, FrameLayout(this))
        val view2 = View.inflate(this, R.layout.onboarding_third_screen, FrameLayout(this))
        val view3 = View.inflate(this, R.layout.onboarding_third_screen, FrameLayout(this))
        val pageList: MutableList<View> = ArrayList()
        pageList.add(view1)
        pageList.add(view2)
        pageList.add(view3)
        return pageList
    }

    fun toggleHideBar(context: AppCompatActivity) {

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        val uiOptions = context.window.decorView.systemUiVisibility
        var newUiOptions = uiOptions
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        val isImmersiveModeEnabled = uiOptions or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY == uiOptions
        if (isImmersiveModeEnabled) {
//            Log.i(TAG, "Turning immersive mode mode off. ")
        } else {
//            Log.i(TAG, "Turning immersive mode mode on.")
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            //            newUiOptions ^= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_LOW_PROFILE
            newUiOptions = newUiOptions xor View.SYSTEM_UI_FLAG_LAYOUT_STABLE

            //            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        context.window.decorView.systemUiVisibility = newUiOptions
        //END_INCLUDE (set_ui_flags)
    }

}