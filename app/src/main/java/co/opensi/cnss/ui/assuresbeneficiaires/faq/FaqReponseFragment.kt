package co.opensi.cnss.ui.assuresbeneficiaires.faq

import android.R.attr.left
import android.R.attr.right
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.R
import co.opensi.cnss.data.models.FaqItem
import co.opensi.cnss.data.models.Section
import co.opensi.cnss.ui.assuresbeneficiaires.prestations.PrestationsAdapter
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleTextItemsAdapter
import co.opensi.cnss.ui.welcome.HomeActivity
import co.opensi.cnss.utils.Functions
import co.opensi.cnss.utils.Functions.Companion.buildDotListContent
import co.opensi.cnss.utils.Functions.Companion.buildTextContent
import kotlinx.android.synthetic.main.fragment_faq.view.*
import kotlinx.android.synthetic.main.fragment_faq_reponse.view.*
import kotlinx.android.synthetic.main.fragment_simple_text_content.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FaqReponseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FaqReponseFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var prestationAdapter: PrestationsAdapter? = null
    private var faqItemsAdapter: FaqItemAdapter? = null
    private var choosenFaqItem: FaqItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_faq_reponse, container, false)
        choosenFaqItem = arguments?.getParcelable("content")!!

        rootView.mainQuestionTV.text = choosenFaqItem!!.content

        (requireActivity()as HomeActivity).setActionBarTitle("FAQ")

        //i want to build programmatically the interface here
        //but it will be a verry complex deal
        //it takes more time to design this than to code it
        // i hate this kind of interfaces

        //we have a big object here a list of sections and subsections

        rootView.satisfactionYes.setOnClickListener {
            rootView.successContent.visibility = View.VISIBLE
            //suppression automatique apres 10 secondes
            object : CountDownTimer(5000, 5000) {
                override fun onTick(l: Long) {}

                override fun onFinish() {
                    rootView.successContent.visibility = View.GONE
                }
            }.start()
        }

        rootView.closeSuccess.setOnClickListener {
            rootView.successContent.visibility = View.GONE
        }

        rootView.satisfactionNo.setOnClickListener {
            //ici on doit afficher la petite fenetre modale avec la question a l'utilisateur
            Toast.makeText(requireContext(), "En cours de développement ...", Toast.LENGTH_LONG).show()

        }


        //we need to add all the sections into the content view

        val view = rootView.content

        data[choosenFaqItem!!.id].forEach {
            if (it.isTextContent) view.addView(buildTextContent(requireActivity(),it.content, it.isBold))
            else if (it.hasTextWithDotList) view.addView(
                buildDotListContent(
                    requireActivity(),
                    it.dotListContent,
                    it.showDot
                )
            )
        }

        return rootView
    }

    companion object {

        val data =

            mutableListOf(

                //La nous sommes dans les prestations familiales
                mutableListOf<Section>(//0
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les prestations familiales comprennent :"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les prestations familiales sont octroyées au travailleur salarié de lui permettre de faire face un tant soit peu aux charges de famille qui ne sont pas prises en compte par l’employeur lors de la détermination du salaire de base. La jouissance de ces prestations est faite sous certaines conditions.\n"
                    )
                ),

                mutableListOf<Section>(//1
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les prestations familiales comprennent :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Les allocations prénatales ;",
                            "Les allocations familiales ;",
                            "Les prestations en nature relatives à l’action sanitaire et sociale."
                        )
                    )

                ),
                mutableListOf<Section>(//2
                    Section(isTextContent = true, isBold = false, content = "Vous devez :"),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "a- Être travailleur salarié(e) pendant au moins six (06) mois sans interruption chez un ou plusieurs employeurs ;",
                            "b- Avoir un ou plusieurs enfants à charge soit son épouse en état de grossesse, soit être une femme salariée en état de grossesse ;",
                            "c- Avoir constitué un dossier à la caisse.\n"
                        )
                    )
                ),
                mutableListOf<Section>(//3
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Une demande de prestations (imprimé gratuitement fourni par la caisse à remplir lisiblement) ;",
                            "Un extrait d’acte de naissance ou jugement supplétif de l’assuré ;",
                            "Un extrait d’acte de naissance de l’épouse ou des épouses ;",
                            "Un extrait d’acte de naissance de vos enfants ;",
                            "Un certificat de vie et de charge ;",
                            "Les certificats de scolarité, d’apprentissage ou d’infirmité pour les enfants à charge",
                            "02 photos d’identité du travailleur salarié"
                        )
                    )
                ),
                mutableListOf<Section>(//4
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "Allocations prénatales :"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Pour en bénéficier, vous devez :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Une demande de prestations (imprimé gratuitement fourni par la caisse à remplir lisiblement) ;",
                            "Un extrait d’acte de naissance ou jugement supplétif de l’assuré ;",
                            "Un extrait d’acte de naissance de l’épouse ou des épouses ;",
                            "Un extrait d’acte de naissance de vos enfants ;",
                            "Un certificat de vie et de charge ;",
                            "Les certificats de scolarité, d’apprentissage ou d’infirmité pour les enfants à charge",
                            "02 photos d’identité du travailleur salarié"
                        )
                    ),
                    Section(isTextContent = true, isBold = true, content = "Montant des droits :"),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "1- Examen du 3ème mois : 1000F",
                            "2- Examen du 6ème mois : 2000F",
                            "3- Examen du 8ème mois : 1500F"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Tout examen non subi fait perdre le droit correspondant."
                    )

                ),
                mutableListOf<Section>(//5
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Avoir travaillé 18 jours ou 120 heures par mois et perçu comme salaire au moins le SMIG ;",
                            "Avoir des enfants à charge âgés de 0 à 21 ans au maximum ;",
                            "Le nombre d’enfants ouvrant droit aux allocations familiales est fixé à 6 au maximum, mais remplaçable progressivement en cas de majorité."
                        )
                    )
                ),
                mutableListOf<Section>(//6
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Un certificat de travail chaque trimestre (imprimé fourni par la caisse) ;",
                            "Un certificat de vie et de charge (chaque début d’année) ;",
                            "Un certificat de scolarité pour chaque enfant âgé d’au moins 6 ans (chaque début d’année scolaire) ;",
                            "Un certificat d’apprentissage pour chaque enfant en apprentissage de 14 à 21 ans (chaque début d’année) ;",
                            "Un certificat médical pour chaque enfant en âge scolaire et infirme ou atteint de maladie incurable ;",
                            "Un certificat médical de visite périodique pour chaque enfant âgé de moins de 6 ans ‘les visites peuvent s’effectuer dans les CMS de la CNSS auprès des Agences Régionales)."
                        )
                    )
                ),
                mutableListOf<Section>(//7
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les allocations familiales se paient à terme échu et par trimestre au taux de 2500 frs par mois et par enfant."
                    ),
                    Section(isTextContent = true, isBold = true, content = "NB"),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Tout événement nouveau (naissance, décès) intervenu dans la famille du salarié doit être déclaré à la CNSS ;\n",
                            "En cas de décès d’un allocataire, la veuve continue de percevoir les allocations familiales jusqu’à la liquidation de la pension survivant orphelin."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "Réclamer donc vos droits avant le délai de 12 mois sinon ils seront prescrits."
                    )
                ),
                mutableListOf<Section>(//8
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Elles comprennent les consultations médicales, les soins médicaux, la fourniture des produits pharmaceutiques, les analyses médicales, les vaccinations, etc. toutes ces prestations sont assurées par la CNSS dans ces centres médico-sociaux."
                    )
                )


                //-------------------------------------------------------------------------------------------------------------
                //La nous sommes au niveau de la pension de vieillesse 9-19

                ,
                mutableListOf<Section>(//9
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les prestations de la branche des pensions comprennent : "
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "La pension de vieillesse",
                            "La pension anticipée",
                            "La pension d’invalidité",
                            "La pension de survivants",
                            "L’allocation de vieillesse",
                            "L’allocation de survivants"
                        )
                    )
                ),
                mutableListOf<Section>(//10
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La pension de vieillesse est un revenu de remplacement qui correspond à une certaine proportion du salaire du travailleur. L’assuré qui atteint l’âge de 60ans a droit à une pension de vieillesse s’il remplit les conditions suivantes : "
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Avoir totalisé au moins 180mois d’assurance effective à la CNSS ;",
                            "Avoir cessé toute activité salariale."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Toutefois, tout assuré qui remplit les conditions plus haut peut également demander la jouissance de ses droits auplus tôt, cinq (05) ans avant l’âge légal de départ à la retraite. Dans ce cas, le montant de la pension subit un abattement de 5% par année d’anticipation. A l’âge de 60ans, cet abattement est supprimé et l’assuré bénéficie de l’intégralité de sa pension."
                    )
                ),

                mutableListOf<Section>(//11
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Tout assuré de la CNSS qui devient invalide avant l’âge de 60ans bénéficie d’une pension d’invalidité." +
                                "Est invalide, tout assuré qui par suite d’une maladie ou d’un accident d’origine non-professionnelle n’est plus capable de percevoir un salaire supérieur au tiers de la rémunération qu’un travailleur de sa catégorie professionnelle pourrait se procurer par son travail.\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Toutefois, les conditions à remplir sont les suivantes : "
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Avoir accompli au moins 60 mois d’assurance ;",
                            "Avoir accompli obligatoirement six (06) mois d’assurance au cours des 12 mois civils précédant le début de l’incapacité ayant conduit à l’invalidité.\n" +
                                    "NB : les conditions ci-dessus ne sont pas exigées si l’invalidité est d’origine accidentelle.\n",
                            "Fournir un certificat médical"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "NB : la pension d’invalidité ne tient pas compte de l’âge de l’assuré. Elle est provisoire et est remplacée par une pension normale quand le bénéficiaire atteint l’âge de 60 ans. Toutefois elle peut être suspendue lorsqu’un contrôle ultérieur relève l’amélioration de l’état morbide du travailleur."
                    )
                ),

                mutableListOf<Section>(//12
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En cas de décès du titulaire d’une pension de vieillesse ou d’invalidité, ainsi qu’en cas de décès d’un assuré qui à la date de son décès, remplissait les conditions pour bénéficier d’une pension de vieillesse ou d’invalidité, les survivants ont droit à une pension de survivants. Sont considérés comme survivants : " +
                                "Est invalide, tout assuré qui par suite d’une maladie ou d’un accident d’origine non-professionnelle n’est plus capable de percevoir un salaire supérieur au tiers de la rémunération qu’un travailleur de sa catégorie professionnelle pourrait se procurer par son travail.\n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "La veuve mariée à condition que le mariage ait été contracté un an au moins avant le décès, à moins qu’un enfant ne soit né de l’union conjugale, ou que la veuve ne se trouve en état de grossesse à la date de décès du conjoint ;",
                            "Le veuf invalide à la charge de l’assurée au titre de l’épouse salariée décédée la première, et à condition que le mariage ait été contracté au moins un an avant le décès de la conjointe ;",
                            "Les enfants à la charge de l’assuré€ décédé€ tels qu’ils sont définis par la réglementation relative aux prestations familiales."
                        )
                    )
                ),

                mutableListOf<Section>(//13
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’allocation de vieillesse est octroyée à l’assuré qui a accompli au moins 12 mois d’assurance et qui, ayant atteint l’âge de 60 ans, cesse toute activité salariée alors qu’il ne satisfait pas à la condition de 180 mois d’assurance pour avoir droit à une pension de vieillesse. Cette allocation est sous la forme d’un versement unique."
                    )
                ),

                mutableListOf<Section>(//14
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Si l’assuré ne pouvait prétendre à une pension d’invalidité et compatit au moins 6 mois et moins de 180 mois d’assurance, à la date de son décès, ses survivants bénéficient d’une allocation de survivants."
                    )
                ),

                mutableListOf<Section>(//15
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Pour déterminer le nombre de mois d’assurance pour la pension d’invalidité, en plus de la période cotisée chaque période de 12 mois comprise entre 60 ans et l’âge de l’invalide, à la date où la pension d’invalidité prend effet est comptée pour 6 mois. Le montant de la pension d’invalidité est calculé selon la formule de la pension de vieillesse.\n" +
                                "\n" +
                                "Les pensions de survivants sont calculées en pourcentage de la pension de vieillesse ou d’invalidité à laquelle l’assuré avait ou aurait eu droit à la date de son décès à raison de : \n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "a- 40% pour le conjoint survivant. En cas de pluralité de veuves, le montant est réparti entre elles à parts égales. Cette répartition est définitive ;",
                            "b- 20% pour chaque enfant orphelin de père ou de mère et 30% pour chaque enfant orphelin de père et de mère dans la limite de 40% de la pension de l’assuré décédé.\n"
                        )
                    )
                ),

                mutableListOf<Section>(//16
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le montant de la pension est le produit de la rémunération mensuelle moyenne et du taux de validation de la pension."
                    ),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "a- La rémunération mensuelle moyenne est le quotient des gains annuels au cours des cinq dernières années ou des gains des 60 derniers mois par 60.",
                            "b- Le taux de pension est composé comme suit :"
                        )
                    ),
                    Section(
                        hasTextWithDotList = true,
                        //showDot = false,
                        dotListContent = mutableListOf<String>(
                            "Une partie fixe de 30% accordée à tous les assurés qui ont totalisé une période d’assurance au moins égale à 180 mois d’assurance.",
                            "Si le total des mois d’assurance ou assimilés dépasse 180 mois, la partie fixe de 30% est majorée de 2% pour chaque période de 12 mois au-delà de 180 mois."
                        )
                    ),
                    Section(isTextContent = true, isBold = true, content = "EXEMPLE"),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Un travailleur qui a une rémunération moyenne de 50000 francs et qui a totalisé 270 mois d’assurance soit 22 ans 6 mois, aura une pension :\n" +
                                "TAUX : 270 mois = 180 mois + 90 mois\n" +
                                "180 mois donnent 30%\n" +
                                "Les 90 mois restants donnent 90 :12 = 7\n" +
                                "Taux = 7 x 2% = 14%\n" +
                                "Taux d pension : 30% + 14% = 44%\n" +
                                "Montant de la pension : 50000 x 44/100=22000f/mois.\n" +
                                "En cas de départ anticipé à la retraite à l’âge de 58 ans par exemple, il aurait perçu une pension anticipée de \n" +
                                "n= 60-58= 2ans\n" +
                                "\n" +
                                "pension anticipée = 22000 – (2 x 5% x 22000)\n" +
                                "\t\t      = 22000-2200 = 19800 \n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "Montant pension anticipée = 19800\n"
                    )

                ),

                mutableListOf<Section>(//17
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En cas de décès de l’assuré, l’allocation est versée aux survivants à charge. A raison de 50% pour le veuf ou les veuves et de 50% pour les enfants à charge. En cas d’absence de veuve(f) les 100% reviennent aux orphelins et vice versa. Mais en l’absence de veuves, veuf et d’orphelins, l’allocation est versée à 100% aux ascendants vivants."
                    )
                ),

                mutableListOf<Section>(//18
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le montant de l’allocation de vieillesse en versement unique est égal au produit de la rémunération mensuelle moyenne par le nombre de 12 mois d’assurance accomplis par l’assuré."
                    )
                ),

                mutableListOf<Section>(//19
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les prestations de la branche des pensions comprennent :\n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Un extrait d’acte de naissance ou jugement supplétif de l’assuré ;\n",
                            "Un extrait d’acte de naissance ou jugement supplétif de chaque épouse ;\n",
                            "Un extrait d’acte de naissance ou jugement supplétif de chaque enfant à charge ;\n",
                            "Les certificats de travail justifiant l’ensemble des activités salariées ;\n",
                            "Le(s) livret(s) d’assurance délivrés par la Caisse ;\n",
                            "La carte IPRAO ;\n",
                            "Une demande de pension ;",
                            "Trois photos d’identité."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En cas de décès, ajouter un acte de décès, un acte de mariage, un acte de non remariage pour la veuve, les certificats de scolarité, de vie et de charge pour les orphelins ;\n" +
                                "En cas d’invalidité, produire un certificat médical attestant l’invalidité.\n"
                    )
                ),

                //-------------------------------------------------------------------------------------------------------------
                //La nous sommes au niveau des risques professionnels 20-31

                mutableListOf<Section>(//20
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La branche des risques professionnels a pour but de prévenir et de réparer les accidents du travail et les maladies professionnelles. Cette réparation se traduit par des prestations en nature et en espèces."
                    )
                ),

                mutableListOf<Section>(//21
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les personnes protégées sont : \n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Les travailleurs salariés ;",
                            "Les fonctionnaires en position de détachement ",
                            "Les élèves des écoles techniques et professionnelles ;",
                            "Les stagiaires et les apprentis mêmes non rémunérés ;",
                            "Les membres des sociétés coopératives de production ainsi que les gérants non-salariés des coopératives et leurs préposés ;",
                            "Les gérants des sociétés à responsabilité limitée et ceux des sociétés de personnes qui ne détiennent pas plus de 50% des parts sociales.\n"
                        )
                    )
                ),

                mutableListOf<Section>(//22
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "C’est un accident survenu par le fait ou à l’occasion du travail, quelle qu’en soit la cause, à tout travailleur relevant du régime général de sécurité sociale. C’est également un accident survenu au travailleur pendant un voyage dont les frais sont à la charge de l’employeur. C’est enfin, un accident survenu au travailleur pendant le trajet de sa résidence au lieu de travail et vice-versa ou pendant le trajet entre le lieu de travail et le lieu où il prend habituellement ses repas et vice-versa dans la mesure où le parcours n’a pas été interrompu ou détourné pour des motifs d’ordre personnel ou indépendants du travail.\n"
                    )
                ),

                mutableListOf<Section>(//23
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "C’est une maladie contractée à l’occasion du travail. Cette maladie doit en principe figurer dans la nomenclature arrêtée par la législation de la sécurité sociale. Deux éléments caractérisent la maladie professionnelle : Le travailleur doit avoir été exposé au risque pendant une certaine période ; Le travailleur doit présenter les symptômes de la maladie dans un délai déterminé. Les déclarations sont recevables même après que le travailleur ait cessé d’être exposé au risque dans la limite des délais d’incubation prévus par les textes."
                    )
                ),

                mutableListOf<Section>(//24
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La victime d’accident du travail doit informer ou faire informer son employeur dans un délai de 24h sauf cas de force majeure, d’impossibilité absolue ou de motif légal. La même obligation incombe aux ayants droit de l’assuré en cas de décès. L’employeur est tenu de déclarer à la CNSS dans un délai de 48h dès qu’il est informé, tout accident du travail et toute maladie professionnelle dont sont victimes les salariés occupés par l’entreprise. L’employeur est tenu de faire assurer les soins de première urgence, d’aviser le médecin de l’entreprise s’il en existe un, d’évacuer la victime vers la formation sanitaire la plus proche, de remettre à la victime une feuille d’accident du travail (imprimés fournis par la CNSS) dûment remplie. Déclarer les accidents du travail à la CNSS dans les 48 heures suivant la survenance de l'événement."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "NB : au-delà de 02 ans la déclaration d’AT/MP est irrecevable."
                    )
                ),

                mutableListOf<Section>(//25
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La victime d’un accident du travail ou d’une maladie professionnelle est tenue : D’observer les prescriptions médicales, de suivre le traitement et de respecter le repos au lit prescrit par le médecin traitant ; De rester à résidence, sauf si son médecin traitant lui permet de se déplacer dans un but thérapeutique ; De ne pas se livrer à un travail rémunéré pendant la période d’incapacité temporaire de travail.\n"
                    )
                ),

                //a performer plutard, trop complexe comme adjencemment
                mutableListOf<Section>(//26
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "1- Les prestations en nature comprennent :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "L’assistance médicale chirurgicale dentaire ;",
                            "Les examens radiologiques ou radiographiques ;",
                            "Les analyses médicales ;",
                            "La fourniture de produits pharmaceutiques ou accessoires ;\n",
                            "L'entretien dans un hôpital ou toute autre formation sanitaire agréée ;\n",
                            "La fourniture, l’entretien et le renouvellement des appareils de prothèse ou d’orthopédie en rapport avec les lésions et reconnus par le médecin conseil de la CNSS ;\n",
                            "La réadaptation fonctionnelle, la rééducation professionnelle et le reclassement de la victime ; le transport de la victime du lieu d’accident à une formation sanitaire ou à sa résidence ;\n",
                            "Les frais funéraires de la victime en cas d’accident mortel."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "2- Les prestations en espèces\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Trois types de prestations peuvent être servies :"
                    ),
                    //la partie ci apres est a performer
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "a- L’indemnité journalière"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Trois types de prestations peuvent être servies :\n" +
                                "NB : l’indemnité journalière est accordée à la victime dès le lendemain de l’AT ou de la MP. Mais au-delà de 360 jours d’Incapacité Temporaire de Travail (ITT), elle est remplacée par une rente provisoire jusqu’à la reprise du travail. L’indemnité journalière est payée à l’employeur qui assure le paiement de la totalité du salaire de la victime.\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "b- Prestations en cas d’Incapacité Permanente Partielle (IPP)\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Trois types de prestations peuvent être servies :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Allocation d’incapacité à versement unique lorsque le taux d’IPP est inférieur à 20%. Elle est égale à 5 fois la rente annuelle fictive.",
                            "Rente viagère pour un taux d’IPP supérieur ou égal à 20%"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "c- Rentes de survivants et ascendants à charge\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Elles sont calculées en % du salaire annuel de la victime dans la limite de 85% et sont réparties comme suit :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Conjoints survivants : 30% ;",
                            "Orphelins : 15% à chacun des 2 premiers, 10% à chacun des deux autres ;",
                            "Père ou mère à charge : 10% à chacun."
                        )
                    )

                ),

                mutableListOf<Section>(//27
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "La rente IPP est déterminée à partir d’un taux utile et d’un salaire annuel utile.\n" +
                                "Le taux utile est égale à la moitié du taux d’incapacité fixé par le médecin conseil de la CNSS, si ce taux est inférieur ou égal à 50%. S’il dépasse 50%, la fraction dépassant les 50% est augmentée de moitié.\n" +
                                "Exemple : Taux d’incapacité fixé à 70% ; jusqu’à 50% le taux utile est de 25%\n" +
                                "Pour les 20% restants du taux d’incapacité, le taux utile correspondant est de 20%+(20% :2) = 30%\n" +
                                "Ce qui revient à dire que le taux utile est de 25%+30% = 55%.\n" +
                                "Le salaire annuel ne peut en aucun cas être inférieur au SMIG multiplié par 1,40. C’est-à-dire 40000 x 12 x1.40 = 672000 (SMIL)\n" +
                                "S’il dépasse 3 fois le SMIL, l’excédent n’est compté que pour moitié. Il n’est pas tenu compte de la fraction dépassant dix fois le SMIL.\n" +
                                "Exemple : salaire réel annuel de la victime = 3000000\n" +
                                "3x SMIL = 672000 x 3 = 2016000\n" +
                                "Salaire utile = 2016000+(3000000-2016000) /2 = 2508000\n" +
                                "Ainsi la rente annuelle serait :\n" +
                                "2508000 x 55% = 1379400\n" +
                                "NB : le rachat de rente n’existe plus.\n"
                    )
                ),

                mutableListOf<Section>(//28
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Déclaration d’accident de travail",
                            "Certificat médical initial",
                            "Les élèves des écoles techniques et professionnelles ;",
                            "Volet n°1 de la feuille d’accident de travail",
                            "Certificat final descriptif (guérison ou consolidation) s’il y a lieu"
                        )
                    )
                ),

                mutableListOf<Section>(//29
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Certificat final descriptif (guérison)",
                            "Rapport d’expertise médicale (médecin conseil de la CNSS)",
                            "Procès-verbal d’enquête (inspection du travail)",
                            "Procès-verbal de constat de la police (s’il y a lieu)",
                            "Le relevé de salaire (12 mois précédant l’accident de travail)",
                            "Extrait d’acte de naissance",
                            "Photographie carte d’identité nationale",
                            "Deux photos d’identité de la victime"
                        )
                    )
                ),

                mutableListOf<Section>(//30

                    Section(isTextContent = true, isBold = true, content = "3-1- conjoint"),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Extrait d’acte de naissance du conjoint\n",
                            "Extrait d’acte de mariage",
                            " Extrait d’acte de décès (victime)\n",
                            "Certificat de non divorce, de non remariage, de non séparation de corps",
                            " Certificat de cause de décès",
                            "Photographie carte d’identité",
                            "Photo d’identité"
                        )
                    ),
                    Section(isTextContent = true, isBold = true, content = "3-2-descendants"),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "extrait d’acte de naissance des enfants",
                            "extrait d’acte de décès (victime)",
                            " procès-verbal du conseil de famille homologué au tribunal\n",
                            "certificat de scolarité (jusqu’à 21 ans)",
                            "certificat d’apprentissage (jusqu’à 21 ans)",
                            "certificat de vie et de charge des enfants",
                            "photographie carte d’identité du tuteur (ne plus produire si la conjointe est désignée comme tutrice des enfants).\n"
                        )
                    ),
                    Section(isTextContent = true, isBold = true, content = "3.3- ascendants\n"),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "extrait d’acte de décès (victime)",
                            "extrait d’acte de naissance ou jugement supplétif",
                            "une enquête sociale attestant que les ascendants étaient à la charge de la victime\n"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "NB : un seul acte de décès si tous les dossiers sont ensembles. Il en est de même pour l’acte de mariage si la conjointe est désignée comme tutrice des enfants\n"
                    )

                ),

                mutableListOf<Section>(//31
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le droit aux indemnités journalières et aux rentes est prescrit respectivement après 6 mois et 7 ans sauf décision contraire de la Commission Permanente. Le médecin conseil de la CNSS peut inviter à tout moment le bénéficiaire de la rente IPP, à subir une expertise médicale afin de constater l’évolution de son état de santé en rapport avec les dommages subis et corriger au besoin, le taux IPP.\n"
                    )
                ),

                //-------------------------------------------------------------------------------------------------------------
                //La nous sommes au niveau du recouvrement des cotisations 32-39

                mutableListOf<Section>(//33
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L'employeur est toute personne physique ou morale privée ou publique qui utilise un (01) ou plusieurs travailleurs contre rémunération. Dès I 'embauche du premier salarié, l'employeur doit matérialiser son existence par son immatriculation à la CNSS.\n" +
                                "A cet effet, il remplit trois (03) sortes d'imprimés que la Caisse met gratuitement à sa disposition.\n" +
                                "Il s’agit : \n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "1- d’une demande d’immatriculation ;",
                            "2- d’un avis d’embauchage ;",
                            "3- d’un état de recensement de son personnel."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’effectif du personnel, la date d’embauche de chaque salarié et les salaires individuels des salariés doivent être mentionnés.\n" +
                                "Ces imprimés dûment remplis doivent être remis à la Caisse sans délai. Celle-ci vous notifie un numéro d’immatriculation que vous aurez à rappeler dans toutes vos correspondances."
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "NB: L’immatriculation de votre entreprise est obligatoire dans l’intérêt de vos travailleurs et dans le vôtre aussi."
                    )
                ),

                mutableListOf<Section>(//34
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Lorsque vous engagez un travailleur déjà affilié à la Caisse, remplissez un avis d’embauchage pour sa prise en charge. N’oubliez pas de mentionner son numéro d’affiliation. Si le travailleur que vous embauchez n’est pas encore affilié, vous adressez à la Caisse un avis d’embauchage obligatoirement accompagné des pièces ci-après :\n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "une (01) photocopie légalisée de la carte d’identité ;",
                            "une (01) photo d’identité ;",
                            "une (01) copie conforme ou photocopie égalisée de l’extrait d’acte de naissance ou du jugement supplétif d’acte de naissance."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Dans tous les cas, le travailleur doit être déclaré à la caisse dès le premier jour de son recrutement.\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "NB : En cas de licenciement ou de résiliation du contrat d’un travailleur, l’employeur le signale à la CNSS par un avis de débauchage.\n"
                    )
                ),

                mutableListOf<Section>(//35
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les cotisations sont constituées des précomptes opérés sur le salaire des travailleurs (part du salarié) complétés de la part patronale dont les proportions sont bien déterminées par les textes en vigueur.\n" +
                                "L’assiette de cotisations est l’ensemble des rémunérations perçues par le travailleur, y compris les primes, indemnités et autres avantages en espèces ainsi que la contrevaleur des avantages en nature.\n" +
                                "Les calculs de cotisations sont faits sur des imprimés de déclarations disponibles à cet effet à la Caisse.\n"
                    )
                ),

                mutableListOf<Section>(//36
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les cotisations sont versées au titre de chacune des branches du régime général de sécurité sociale gérées par la CNSS.\n" +
                                "Les taux de cotisation sont : \n"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        showDot = false,
                        dotListContent = mutableListOf<String>(
                            "- Prestations familiales : 9% à la charge de l’employeur ;",
                            "- Risques professionnels : 1 à 4% à la charge de l’employeur ; le taux des risques professionnels varie selon la nature de l’activité de l’entreprise ;\n",
                            "- Pension : 10% dont :\n"
                        )
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "6,4% à la charge de l’employeur ;\n",
                            "3,6% à la charge du travailleur.\n"
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "En ce qui concerne les apprentis et les \n" +
                                "élèves des écoles professionnelles, seule la\n" +
                                "cotisation des risques professionnels est \n" +
                                "versée.\n"
                    )
                ),

                mutableListOf<Section>(//37
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur est débiteur vis-à-vis de la Caisse de la cotisation totale y compris la part du salarié qui est précomptée dur la rémunération de celui-ci lors de chaque paie. L’employeur doit produire lors du versement des cotisations, les pièces justificatives suivantes :"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "une déclaration de salaires et d cotisations ",
                            "une déclaration nominative du personnel à fournir trimestriellement par les employeurs utilisant au moins vingt (20) salariés.\n"
                        )
                    )
                ),

                mutableListOf<Section>(//38
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le versement des cotisations est mensuel pour les employeurs occupant 20 salariés au moins.\n" +
                                "Le versement est trimestriel pour les employeurs occupant moins de 20 salariés.\n" +
                                "Les cotisations sont versées par l’employeur à la CNSS dans les 15 jours qui suivent la période pour laquelle elles sont dues.\n" +
                                "En cas de cessation d’activité ou de cession de l’entreprise, les cotisations sont immédiatement exigibles.\n"
                    )
                ),

                mutableListOf<Section>(//39
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur qui ne verse pas les cotisations dans le délai prescrit est passible d’une majoration de 1.5% par mois ou fraction de mois de retard du montant des cotisations.\n" +
                                "Cette majoration est due au même titre que la cotisation et payée dans les mêmes conditions.\n"
                    )
                ),

                mutableListOf<Section>(//40
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "L’employeur qui a contrevenu aux prescriptions légales en matière de sécurité sociale peut être poursuivi par la Caisse. Tout employeur débiteur des cotisations peut se voir opposer des saisies- arrêts pratiquées à la requête de la Caisse.\n"
                    )
                ),

                //-------------------------------------------------------------------------------------------------------------
                //La nous sommes au niveau du droit des travailleurs migrants 40-45


                mutableListOf<Section>(//41
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Le travailleur migrant est toute personne ayant accompli ses périodes d'activité dans un ou plusieurs pays y compris ou non le Bénin."
                    )
                ),


                mutableListOf<Section>(//42
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Pays pris en compte par  les conventions : \n" +
                                "Bénin\n" +
                                "Burkina-Faso\n" +
                                "Niger\n" +
                                "Togo\n" +
                                "Tchad\n" +
                                "République Centrafricaine \n" +
                                "La France\n" +
                                "Les pays membres de la Société multinationale Air Afrique.\n" +
                                "Pays pris en compte par les accord inter caisses:\n" +
                                "Niger \n" +
                                "Côte d’Ivoire  \n" +
                                "Burkina Faso\n" +
                                "Le Sénégal \n" +
                                "Togo"
                    )
                ),


                mutableListOf<Section>(//43
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Les conventions et les Accords inter caisses en matière de sécurité sociale permettent aux travailleurs migrants de bénéficier de leurs prestations."
                    )
                ),

                mutableListOf<Section>(//44
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Vous devez avoir travaillé durant toute votre carrière dans un ou plusieurs pays étrangers liés au Bénin par une convention ;",
                            "Vous devez avoir effectué une période de votre carrière au Bénin et une période dans un ou plusieurs pays liés au Bénin par une convention.\n"
                        )
                    )
                ),

                mutableListOf<Section>(//45
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "Prestation à long terme\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Ce sont des droits qui concernent les rentes et les pensions. "
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "Prestation à court terme\n"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Ce sont les prestations familiales qui vous sont garanties dans le cadre de l’application de ces instruments de coordination lorsque votre famille réside au Bénin alors que vous travaillez dans un pays lié au vôtre par une convention."
                    )
                ),

                mutableListOf<Section>(//46
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "1- Cas d’une carrière passée hors du Bénin"
                    ),
                    Section(
                        hasTextWithDotList = true,
                        dotListContent = mutableListOf<String>(
                            "Le dossier peut être instruit et liquidé par le pays d’accueil ;",
                            "Le dossier peut également, sur la base de votre demande adressée à la CNSS être instruit par le Bénin et liquidé par votre pays d’accueil."
                        )
                    ),
                    Section(
                        isTextContent = true,
                        isBold = true,
                        content = "2- Cas d’une carrière effectuée en partie au Bénin et en partie dans un ou plusieurs pays liés au Bénin par une Convention"
                    ),
                    Section(
                        isTextContent = true,
                        isBold = false,
                        content = "Chaque pays tient compte de la période aussi bien travaillée sur son propre territoire que le territoire du ou des pays d’accueil, pour déterminer la durée d’immatriculation et les périodes d’assurance.\n" +
                                "Toutefois, les droits sont liquidés au prorata de la période travaillée dans chacun des pays.\n" +
                                "NB : Dans le cas des conventions bilatérales, c’est le lieu de résidence qui détermine l’institution, qui se charge de l’instruction et de la liquidation de votre dossier.\n" +
                                "Dans tous les cas, vous bénéficierez grâce aux conventions et aux accords inter caisses de la totalité de vos droits qui vous sont payés dans votre pays de résidence."
                    )
                )


            )





        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FaqReponseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FaqFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}