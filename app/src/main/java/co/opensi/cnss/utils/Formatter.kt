package co.opensi.cnss.utils

open class NumericFormatter(stringPattern: String, charNumberRepresentation: Char) {
    protected val pattern: String = stringPattern.trim().takeIf {
        it.length > 1
    } ?: throw IllegalArgumentException("String length must be upper than 1")
    private val numberRepresentation: Char = charNumberRepresentation.takeIf {
        it.isLetter() && pattern.contains(it)
    }
        ?: throw IllegalArgumentException("The character must be a letter and contained in the pattern")

    operator fun invoke(numericString: String): String {
        val rawNumeric = numericString.replace("[^0-9]".toRegex(), "")
        return if (rawNumeric.isNotBlank()) formattedNumeric(rawNumeric) else ""
    }

    private fun formattedNumeric(rawNumeric: String) = buildString {
        var currentIndexInRawNumeric = 0

        pattern.forEach { char ->
            if (currentIndexInRawNumeric > (rawNumeric.length - 1)) return@forEach
            append(
                if (char == numberRepresentation) {
                    rawNumeric[currentIndexInRawNumeric].also {
                        currentIndexInRawNumeric++
                    }
                } else char
            )
        }
    }

    fun rawNumeric(formattedNumeric: String) =
        formattedNumeric.trim().replace("[0-9]".toRegex(), "").toSet()
            .joinToString(separator = "").replace("-", "").let {
                val intermediateResult = formattedNumeric.replace("-", "")
                intermediateResult.replace("[$it]".toRegex(), "")
            }
}

class NumericDateFormatter (stringPattern: String, charNumberRepresentation: Char) :
    NumericFormatter(stringPattern, charNumberRepresentation) {
    fun isValidate(input: String) = pattern.length == input.trim().length
}