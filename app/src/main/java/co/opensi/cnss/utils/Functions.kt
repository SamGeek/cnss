package co.opensi.cnss.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.text.*
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.opensi.cnss.ui.employeurs.recouvrement.SimpleTextItemsAdapter
import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan
import io.github.inflationx.calligraphy3.TypefaceUtils
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import java.util.Calendar.*

class Functions {

    companion object{

        fun getTypeFace(context: Context, isBold: Boolean): CalligraphyTypefaceSpan {
            return CalligraphyTypefaceSpan(
                TypefaceUtils.load(
                    context.getAssets(),
                    if (isBold == true) "fonts/Montserrat-Bold.ttf" else "fonts/Montserrat-Regular.ttf"
                )
            )
        }
        fun setBoldTextView(textView: TextView, context: Context, isBold: Boolean) {
            val sBuilderSecond = SpannableStringBuilder()
            sBuilderSecond.append(textView.text)
            sBuilderSecond.setSpan(
                getTypeFace(context, isBold),
                0,
                textView.text.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.setText(sBuilderSecond, TextView.BufferType.SPANNABLE)
        }



        var load: ProgressDialog? = null

        fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
            this.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(editable: Editable?) {
                    afterTextChanged.invoke(editable.toString())
                }
            })
        }

        fun getFullFormatted(
            text: String?
        ): String? {
            return if (text == null ) "" else  text?.substring(0, 1).toUpperCase() + text?.substring(
                1
            ).toLowerCase()
        }

        fun completeZeros(text: String) : String{
            return if (text.length<2) "0"+text else text;
        }

        fun hideKeyboard(activity: Activity, view: View) {
            val imm: InputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }




        fun getDiffYears(first: Date?): Int {
            val a: Calendar = getCalendar(first)
            val b: Calendar = getCalendar(Calendar.getInstance().time)
            var diff: Int = b.get(YEAR) - a.get(YEAR)
            if (a.get(MONTH) > b.get(MONTH) ||
                a.get(MONTH) === b.get(MONTH) && a.get(DATE) > b.get(DATE)
            ) {
                diff--
            }
            return diff
        }

        fun isEmpty(edt: EditText): Boolean {
            return edt.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)
        }


        fun showProgress(mContext: Context) {
            load = ProgressDialog(mContext)
            if (null == load) {
                load = ProgressDialog(mContext)
            }
            load!!.setCancelable(false)
            load!!.show()
        }

        fun hideProgress() {
            if (null != load) load!!.dismiss()
            load = null
        }


        fun isValidEmail(target: String?): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(
                target
            ).matches()
        }

        fun roundDecimal(number: Double): Double? {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(number).toDouble()
        }



        fun checkEmptyFields(ed: EditText, errMsg: String = "Veuillez renseigner ce champ"): Boolean {

            var verif = true

            if (ed.getText().toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
                ed.setError(errMsg)
                verif = false
            }else{
                ed.setError(null)
            }

            return verif
        }

        fun getCalendar(date: Date?): Calendar {
            val cal: Calendar = Calendar.getInstance(Locale.FRENCH)
            cal.setTime(date)
            return cal
        }


        fun toastLong(mContext: Context, message: String) {
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show()
        }

        fun toastShort(mContext: Context, message: String) {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
        }

        fun buildDotListContent(
            activity: Activity,
            dotListContent: MutableList<String>,
            showDot: Boolean
        ): View? {
            val recyclerView =  RecyclerView(activity)
            var listItemsAdapter: SimpleTextItemsAdapter? = null

            //on doit wrap ce composant dans du nested scroll pour eviter les scrolls
            val nestedScrollView = NestedScrollView(activity)
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            nestedScrollView.setLayoutParams(layoutParams)

            listItemsAdapter = SimpleTextItemsAdapter(
                dotListContent,
                activity as AppCompatActivity,
                object : SimpleTextItemsAdapter.Listener {
                    override fun onItemChoosen(textIcon: String) {
                    }
                },
                showDot
            )

            val ccLayoutManager = LinearLayoutManager(activity)
            recyclerView.apply {
                layoutManager = ccLayoutManager
                itemAnimator = DefaultItemAnimator()
                adapter = listItemsAdapter
            }

            nestedScrollView.addView(recyclerView)

            return nestedScrollView
        }

        fun buildTextContent(activity: Activity, content: String, isBold: Boolean): View {
            val textView = TextView(activity)
            textView.textSize = 12f

            textView.setTextColor(Color.parseColor("#000000"))
            //textView.setTypeface(Typeface.DEFAULT)
            val builder = SpannableStringBuilder()
            builder.append(content)
            builder.setSpan(
                Functions.getTypeFace(activity, isBold),
                0,
                content.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.setText(builder, TextView.BufferType.SPANNABLE)

            val params = LinearLayout.LayoutParams(
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT,
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0, 10, 0, 10)
            textView.setLayoutParams(params)

            return textView
        }

        fun dpToPx(context: Context,dp: Int): Int {
            return (dp * context.resources.displayMetrics.density).toInt()
        }

        fun pxToDp(context: Context,px: Int): Int {
            return (px / context.resources.displayMetrics.density).toInt()
        }

    }

}