package co.opensi.cnss.data.models

import android.os.Parcel
import android.os.Parcelable

open class Prestation(var id :Int =0, var photo :Int =0,var libelle :String ="") : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(photo)
        parcel.writeString(libelle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Prestation> {
        override fun createFromParcel(parcel: Parcel): Prestation {
            return Prestation(parcel)
        }

        override fun newArray(size: Int): Array<Prestation?> {
            return arrayOfNulls(size)
        }
    }
}