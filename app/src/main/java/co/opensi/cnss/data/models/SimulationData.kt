package co.opensi.cnss.data.models

import android.os.Parcel
import android.os.Parcelable

open class SimulationData(
    var title: String = "",
    var hasThirdText: Boolean = false,
    var firstText :  String = "",
    var secondText :  String = "",
    var thirdText :  String = "",
    var headerTextIcon :  TextIcon?=null,
    var option :  String = ""


) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readParcelable(TextIcon::class.java.classLoader)!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (hasThirdText) 1 else 0)
        parcel.writeString(firstText)
        parcel.writeString(secondText)
        parcel.writeString(thirdText)
        parcel.writeString(option)
        parcel.writeParcelable(headerTextIcon, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SimulationData> {
        override fun createFromParcel(parcel: Parcel): SimulationData {
            return SimulationData(parcel)
        }

        override fun newArray(size: Int): Array<SimulationData?> {
            return arrayOfNulls(size)
        }
    }
}