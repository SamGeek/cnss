package co.opensi.cnss.data.service.local

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Samuel on 30/05/20.
 */
class AppPreferencesHelper (
    context: Context,
    prefFileName: String
) {
    private val mPrefs: SharedPreferences

    var isOnboardingDone: Boolean?
        get() = if (mPrefs.contains(IS_ONBOARDING_DONE)) {
            mPrefs.getBoolean(IS_ONBOARDING_DONE, true)
        } else false
        set(isFirstConnect) {
            var sharedPreferencesEditor = mPrefs.edit()
            sharedPreferencesEditor.putBoolean(IS_ONBOARDING_DONE,  isFirstConnect!!)
            sharedPreferencesEditor.apply()
        }

    companion object {
        private const val IS_ONBOARDING_DONE = "IS_ONBOARDING_DONE"
    }

    init {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
    }
}