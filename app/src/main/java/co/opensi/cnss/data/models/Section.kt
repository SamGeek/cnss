package co.opensi.cnss.data.models

open class Section(
    var id :Int =0,
    var isTextContent: Boolean = false,
    var content: String = "",
    var hasTextWithDotList: Boolean = false,
    var showDot: Boolean = true,
    var isBold: Boolean = false,
    var dotListContent: MutableList<String> = mutableListOf<String>()
)
