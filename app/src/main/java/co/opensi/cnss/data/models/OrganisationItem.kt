package co.opensi.cnss.data.models

import android.os.Parcel
import android.os.Parcelable

open class OrganisationItem(
    var title: String = "",
    var content: String = ""


) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrganisationItem> {
        override fun createFromParcel(parcel: Parcel): OrganisationItem {
            return OrganisationItem(parcel)
        }

        override fun newArray(size: Int): Array<OrganisationItem?> {
            return arrayOfNulls(size)
        }
    }
}