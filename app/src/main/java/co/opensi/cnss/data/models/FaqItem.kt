package co.opensi.cnss.data.models

import android.os.Parcel
import android.os.Parcelable

open class FaqItem(
    var id :Int =0,
    var content: String = ""


) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FaqItem> {
        override fun createFromParcel(parcel: Parcel): FaqItem {
            return FaqItem(parcel)
        }

        override fun newArray(size: Int): Array<FaqItem?> {
            return arrayOfNulls(size)
        }
    }
}