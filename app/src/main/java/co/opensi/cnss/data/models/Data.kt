package co.opensi.cnss.data.models

import android.os.Parcel
import android.os.Parcelable

open class Data(
    var title: String = "",
    var isActive: Boolean = true,
    var isTextContent: Boolean = false,
    var isListContent: Boolean = false,
    var isComplexListContent: Boolean = false,
    var hideTextContent: Boolean = false,
    var hasNotaBene: Boolean = false,
    var isSimpleTextTiming: Boolean = false,
    var hasOnlyTextDetails: Boolean = false,
    var onlyTextDetails: String = "",
    var hasTextWithDotList: Boolean = false,
    var hasHeaderImage: Boolean = false,
    var headerImage: Int = 0,
    var hasAdditionnalTextFirst: Boolean = false,
    var hasAdditionnalTextSecond: Boolean = false,
    var additionnalTextFirst: String = "",
    var additionnalTextSecond: String = "",
    var listContent: MutableList<TextIcon> = mutableListOf(),
    var dotListContent: MutableList<String> = mutableListOf(),
    var notaBeneContent: String = "",
    var textContent: String = "",
    var tabItemName: String = "",
    var complexData: ComplexData? = null,
    var isFlexibleData: Boolean = false,
    var flexibleContent : MutableList<Section> =  mutableListOf()

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
//        TODO("listContent"),
//        TODO("dotListContent"),
//
//        parcel.readString(),
//        parcel.readString(),
//        parcel.readParcelable(ComplexData::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeByte(if (isTextContent) 1 else 0)
        parcel.writeByte(if (isListContent) 1 else 0)
        parcel.writeByte(if (isComplexListContent) 1 else 0)
        parcel.writeByte(if (hideTextContent) 1 else 0)
        parcel.writeByte(if (hasNotaBene) 1 else 0)
        parcel.writeByte(if (isSimpleTextTiming) 1 else 0)
        parcel.writeByte(if (hasTextWithDotList) 1 else 0)
        parcel.writeString(notaBeneContent)
        parcel.writeString(textContent)
        parcel.writeString(tabItemName)
        parcel.writeParcelable(complexData, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }
}

open class TextIcon(var icon: Int, var texte: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        TODO("Not yet implemented")
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<TextIcon> {
        override fun createFromParcel(parcel: Parcel): TextIcon {
            return TextIcon(parcel)
        }

        override fun newArray(size: Int): Array<TextIcon?> {
            return arrayOfNulls(size)
        }
    }
}

open class ComplexData(
    var simpleTextFirst: String = "",
    var boldTextFirst: String = "",
    var hasChoiceContent: Boolean = false,
    var boldTextSecond: String = "",
    var hasSeparatorSecond: Boolean = false,
    var hasSimpleTextSecond: Boolean = false,
    var shouldSimpleTextSecondBold: Boolean = false,
    var simpleTextSecond: String = "",
    var hasRecyclerThird: Boolean = true,
    var choiceOptionFirst: String = "",
    var choiceOptionSecond: String = "",
    var recyclerFirstContent: MutableList<TextIcon> = mutableListOf<TextIcon>(),
    var recyclerSecondContent: MutableList<TextIcon> = mutableListOf<TextIcon>(),
    var recyclerSecondBisContent: MutableList<TextIcon> = mutableListOf<TextIcon>(),
    var recyclerThirdContent: MutableList<TextIcon> = mutableListOf<TextIcon>()
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        TODO("listContent"),
        TODO("listContent"),
        TODO("listContent")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(simpleTextFirst)
        parcel.writeString(boldTextFirst)
        parcel.writeByte(if (hasChoiceContent) 1 else 0)
        parcel.writeString(boldTextSecond)
        parcel.writeByte(if (hasSeparatorSecond) 1 else 0)
        parcel.writeByte(if (hasSimpleTextSecond) 1 else 0)
        parcel.writeByte(if (shouldSimpleTextSecondBold) 1 else 0)
        parcel.writeString(simpleTextSecond)
        parcel.writeByte(if (hasRecyclerThird) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ComplexData> {
        override fun createFromParcel(parcel: Parcel): ComplexData {
            return ComplexData(parcel)
        }

        override fun newArray(size: Int): Array<ComplexData?> {
            return arrayOfNulls(size)
        }
    }
}